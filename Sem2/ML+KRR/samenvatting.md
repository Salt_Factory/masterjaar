*Disclaimer*: ik denk dat ik deze samenvating up-to-date heb gehouden, maar ik weet het nimeer zeker.

# 1. Zoekproblemen
## 1.1 Inleiding

Zoekproblemen zijn deel van het onderzoeksveld van Operationeel Onderzoek (OO).

**"De toepassing van wetenschappelijke methoden op complexe problemen die te maken hebben met het beheer van omvangrijke systemen van mensen, machines, middelen in de industrie, handel, overheid en defensie."**

Een paar gezellige definities:

**Feasible-Infeasible**: Oplosbaar-onoplosbaar. Een feasable oplossing van een probleem voldoet aan alle harde beperkingen van dat probleem.

**Harde beperking**: Beperking waaraan een oplossing MOET voldoen.

**Zachte beperking**: Beperking waaraan bij voorkeur voldaan is (maar dit niet noodzakelijk is om feasible te zijn).

**Evaluatiefunctie**: Doelfuntie, objectieffunctie, kostfunctie. De kwaliteit van een oplossing hangt onder andere af van de mate waarin aan de zachte beperkingen voldaan is -> dit berekenen we met een doelfunctie.

**Deterministisch zoeken**: Zoekmethode die altijd dezelfde oplossing vindt voor een probleem. <->: niet-deterministisch zoeken

**Optimalisatie**: Het proces dat de beste oplossing probeert te vinden uit alle beschikbare oplossingen. Hierbij is een evaluatiefunctie nodig die de kwaliteit van een oplossing bepaalt.

**Lokaal en globaal optimum**: Oplossing waarvan alle 'buren' in de zoekruimte slechter zijn. Het globaal optimum is de allerbeste oplossing modelijk. 

**Exhaustief zoeken**: Alle mogelijke oplossingen zoeken en de beste selecteren.

**Heuristieken**: Wanneer exhaustief zoeken onmogelijk is, moeten we een methode vinden om kwaliteitsvolle oplossing te bekomen zonder daarvoor alle oplossingen te moeten doorlopen: een heuristiek.

* Constructieve heurstieken dienen om een initiele oplossing te construeren van het begin af
* Lokale zoekmethoden beschouwen de buren als potentiele vervangers. Wanner we een nieuwe oplossing uit de buurt aanvaarden, maken we een zet van de huidige oplossing naar de nieuwe.
* Hill climbing/Steepest descent genereert een oplossing uit de buurt, en gaat ernaartoe enkel wanneer de waarde van de evaluatiefunctie beter is dan de huidige waarde.

**Metaheuristieken**: Strategie die andere heuristieken leidt en aanpast om betere oplossingen te genereren dan deze die met lokale zoektechnieken te bereiken zijn.

**Matheuristics**: heuristieken die lokaal zoeken mbv optimale wiskundige methoden.

## 1.2 Wiskundige optimalisatietechnieken
Deel van Matheuristics?
Het zijn wiskundige technieken voor het oplossen van praktische problemen, ontstaat in de jaren 50 en worden nog steeds gebruikt (al dan niet als bouwblok).

*	Lineair programmeren
*	Geheeltallig programmeren
	-	Lineair programmeren
	-	Dynamisch programmeren
	-	Branch & Bound
	

### 1.2.1 Lineair programmeren
Een lineair optimalisatieprobleem is een probleem waarin de doelfunctie en de beperkingen voor te stellen zijn in de vorm van lineaire uitdrukkingen van de beslissingsvariabelen.

Wanneer een probleem n variabelen heeft, definieren de  beperkingen een verzameling hypervlakken in een n-dimentionale ruimte. Hier vormen ze een gebied dat feasible oplossingen definieert.

Het feasible gebied is een convexe ruimte, en de doelfunctie een convexe functie. Wanneer de variabelen niet negatief zijn, bevindt de optimale oplossing zich op een extreem punt van de oplossingenruimte.
Het 'omhulsel' dat gevormd wordt door de beperkingen noemen we de simplex.

Er zijn concreet twee categorieen oplossingsmethoden:

*	Simplex methoden: zoekt naar de uiterste punten in het feasible gebied. Eenvoudig te implementeren, maar kan heel inefficient zijn.
* Interior point methoden: zoeken een pad van oplossingen door het binnenste van het feasible gebied, zodanig dat wanneer ze de buitenkant bereiken de gevonden oplossing optimaal is.

### 1.2.2 Geheeltallig programmeren

Gelijkaardig aan lineair programmeren, maar dan met geheeltallige variabelen.

* Voordeel: aantal problemen dat gemodeleerd kan worden is veel groter
* Nadeel: het is veel moeilijker op te lossen

Manieren om het te versimpelen zijn creatieve formuleren:

*	Geheeltallige hoeveelheden
*	Binaire beslissingen
*	Vaste kosten
*	Logische beperkingen
*	Sequentieproblemen


Belangrijk om succesvol te modelleren is formulering en relaxatie.
Lineaire relaxatie is het bekomen van een lineair programma, door geheeltallige beperkingen te laten vallen.
Wanneer LR infeasible is, is IP dat ook. LR levert grenzen voor de optimale waarde van IP. 

Voor het oplossen van de IP kunnen we afronden, en gebruik maken van de branch & bound methode.
Het voordeel hiervan is dat IP een breder toepassingsbereik heeft dan LP. Nadelen zijn dat B&B niet altijd snel leidt tot een oplossing, en wanneer er nergens een bound mogelijk is er 2^n deelproblemen zijn.

Binaire beslissingen: oftewel ja, oftewel nee.

Vaste kosten: bv productiekosten uitdrukken met een extra binaire variabele

Logsche beperkingen: ??

Sequentieproblemen: uitdrukken wanneer dat bv een machine niet tegelijkertijd door 2 dingen gebruikt kan worden


Probleem: het is niet eenvoudig snijvlakken te vinden die een oplossingsruimte kleiner maken
Algemene procedure: Gomory-Chvatal

1. Neem een of meer beperkingen, vermenigvuldig ze elk met een niet-negatieve constante en tel ze op in een enkelvoudige beperking
2. rond elke coefficient in het linkerlid af naar beneden
3. rond het rechterlid van de beperking af naar beneden

De nieuwe beperking vormt een snijvlak wanneer de afronding in het rechterlid een grotere impact heeft dan de afronding in het linkerlid. Vooraleer nuttig wanneer er geen afronding nodig is in de tweede stap.

Ook belangrijk: symmetrie vermijden! Mogelijk door:

* Beperkingen toe te voegen
* variabelen vast te leggen
* de formuleren aan te passen

## 1.3 Heuristieken
**Heuristieken zijn informele methoden die toepasbaar zijn op complexe problemen en dienen om 'voldoende goede' oplossing te bekomen.**
Nadelen:

* Geen garantie voor het vinden van de optimale oplossing
* vaak enkel bruikbaar om de problemen waarvoor ze ontwikkeld zijn op te lossen
* leiden mogelijks tot een slechte oplossing

Heuristiek = vuistregel

* Constructieve heuristieken: opbouwen van een oplossing
* Perturbatieve heuristieken: wijzigen van een oplossing

### 1.3.1 Lokaal zoeken

Problemen oplossen waarvoor de exacte mathematische methoden te traag werken. Een voordeel is dat ze een oplossing bekomen in een cava tijd. Een nadeel is dat er geen garantie is over de kwaliteit van de oplossing.

**Oplossingenruimte**: De verzameling van alle mogelijke oplossingen.

**Neighbourhood of omgeving**: N(S<sub>k</sub>) van een oplossing S<sub>k</sub>: deelverzameling van de oplossingenruimte met oplossingen bekomen door heuristieken toe te passen.

Algoritme:

1. Initialiseren, startoplossing maken
2. Kiezen en vervangen: nieuwe oplossing uit omgeving kiezen en vervangen, stoppen wanneer er geen enkel element is dat aan de criteria voldoet
3. stoppen wanneer voldaan aan de voorwaarden

#### Voorstelling van oplossingen:

* zo eenvoudig mogelijk
* zo snel/eenvoudig mogelijk te manipuleren
* zo snel/eenvoudig mogelijk te evalueren

Mogelijkheden: binaire codering, geheeltallige codering, permutatie, floating-point voorstelling, niet-lineaire voorstelling.
Of indericte voorstellingen: jobgebaseerd, operatiegebaseerd, ...

Een voorstelling dient te voldoen aan:

* Volledigheid
* Connectiviteit
* Efficientie

#### Omgeving

Bitflip bij binaire voorstelling

Permutaties

#### Zoekproces

**Stap 1**: Hoe initiele bepalen? Starten van een gekende oplossing, een eenvoudige oplossing of willekeurig

**Stap 2**: Kies en vervang: welke oplossing kiezen? Beste verbetering, eerste verbetering, willekeurige selectie, gebaseerd op kennis, ... Eventueel tournament factor.

**Intensificatie:** exploiteer de beste gevonden oplossing

**Diversificatie:** Exploreer de zoekruimte


#### Lokale optima vermijden
metaheuristieken zoals itereren met verschillende oplossingen, het landschap van de zoekruimte veranderen, niet-verbeterende oplossingen accepteren

**Stap 3**: stopcriterium: absolute rekentijd, tijd na laatste verbetering, aantal iteraties na laatste verbetering, 

## 1.4 Metaheuristieken

1. Sochastische metaheuristieken - Simulated Annealing
2. Geheugengebaseerde metaheuristieken - Tabu search
3. Populatiegebaseerde metaheuristieken - Genetische algoritmen

**Een metaheuristiek is een methode die een probleem optimaliseert door 
iteratief te proberen een kandidaatoplossing te verbeteren met betrekking
tot een gegeven kwaliteitsmaat. Metaheuristieken zijn niet
probleemspecifiek. Ze zijn in staat om grote ruimten met
kandidaatoplossingen te doorzoeken, zonder garantie op het vinden van een optimale oplossing.**

### 1.4.1 Stochastische metaheuristieken

Simulated annealing: gebaseerd op het proces van koelen en kristallisatie
Bij het zoeken van een volgende neighbour gaan we een random neighbour maken, en zijn kwaliteit berekenen. Indien de kwaliteit hoger ligt, nemen we deze als nieuwe oplossing. Indien de kwaliteit lager ligt, is er een willekeurige kans dat we het alsnog accepteren.

parameters te bepalen:

* initiele temperatuur
* evenwichtstoestand
* koeling
* stopconditie

Methoden gebaseerd op SA:

* Threshols accepting
	- Great deluge
	- Record-to-record
* Demon algorithms

### 1.4.2 Geheugengebaseerde metaheuristieken

**Tabu search**: metaheuristiek met geheugen, voorkomt vastlopen in lokaal optimum

Algoritme sturen zodat het een zet van elke oplossing naar elke buur kan uitvoeren (zelfs als slechter). Dit zorgt ervoor dat er een gevaar onstaat om in een lus vast te zitten. Tabu Search gebruikt een geheugenstructuur die zeten verbiedt wanneer ze terug ouden leiden naar een recent bezochte oplossing.

Het heeft drie soorten geheugen:

* tabu-lijst: voorkomt lussen
* middellangetermin: intensificatie
* langetermijn: diversificatie

### 1.4.3 Genetische algoritmen

Gebaseerd op the one and only Charles Darwin!

Merci Charles.

# 2 Artificiele Intelligentie

Wat is AI? 

**AI is het onderzoeksdomein dat zich bezighoudt met taken die vandaag de dag nog te moeilijk zijn voor een computer.**

Vennekens zijn lievelingsdefinitie:
**Algemene patronen van menselijk denken/complex gedrag met compacte, hoog-niveau instructies.**

Twee grote topics in AI:

1. Inductie: machine learning, datamining
2. Deductie: kennisrepresentatie en redeneren

# 2.1 Inleiding: verschillende soorten ML

* supervised vs unsupervised
* binaire classificatie: neuraal netwerk, Bayesiaans netwerk, SVM, beslissingsboom, ...
* regressie: Lineaire regressie, Bayesiaans netwerk, neuraal netwerk, regressieboom, ...
* unsupervised: clustering, pattern mining, Bayesian network learning
* semi-supervised
* active learning: Q learning
* white box vs black bo

# 2.2 Beslissingsbomen

Data = training set (70%) en test set (30%)

We gebruiken de test set op het model te evalueren:
Accuracy = TP + TN /(TP+TN+FP+FN)

ID3 algoritme:

* Begin met wortel
* Maak een lijst TODO vol met bladeren die nog gesplitst moeten worden
* kies blad uit TODO, kies beste feature, splits blad
* Voordeel: kleine bomen, Occam's razor

Welke bladeren splitsen? Splits tot 100% puurheid

Wek criterium voor splitsing? Kies criterium dat puurheid meeste vergroot

Hoe puurheid meten? Entropy

**Maar!** Gevaar voor overfitting

Alternaief: splits training set in training + validatie, bereken o accuracy op validatie set nog steeds zou stijgen bij split

Wat met numerieke variabelen? Continu of discreet: opvoorhand discretiseren (nadeel: hoe grenzen leggen?).

Gebruik van bomen: greedy betekent mogelijk niet-optimaal en van gecorreleerde features wordt er typisch maar 1 gebruikt.
Voordelen: effient, redelijk flexibel en robuust

Uitbreidingen: random forests, boosting
