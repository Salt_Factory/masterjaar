# Smart Embedded Electronics
## Error Analyis: Sources of Errors
#### Error analyse:

1.	Identificeer oorzaken van error
2.	schat de grootte van de fout
3.	bepaal hoe de fout te minimaliseren en controleren

**Significante cijfers:** Dit zijn de cijfers van een getal welke we met zekerheid kunnen gebruiken.

*	Numerische methoden zijn benaderingen, er is nood aan criteria om zekerheid uit te drukken
*	Een getal zoals pi kan nooit exact weergegeven worden: hier is een **round-off error**.

**Precision**: Hoe dicht individuele waarden bij elkaar liggen (kleine of grote spread op de waarden)

**Accuracy**: Hoe dicht de waarden bij de turewaarde (de echte waarde) liggen.

True error Et = true value - approximation

| Naam 	| Symbool  	| Formule 	| Uitleg|
|---	|---	|---	| --- |
|  True error (absolute)	| E<sub>t</sub> 	| true value - approximation 	| Kan enkel berekend worden indien de true solution bekend is. Houdt geen rekening met relatieve magnitude|
|  True (normalized) relative error 	|  ε<sub>t</sub>	|  true error(absolute)/true value * 100%	| Gebruiken wanneer de true solution niet bekend is.
|  Estimated (normalized) relative error	|  ε<sub>a</sub>		|  approximation error/approximation * 100%	|
|  Estimated (normalized) iterative relative error	|  ε<sub>a</sub> 	| (current approximation - previous)/current * 100%  	|
|  Stopping error|  ε<sub>s</sub> 	| /  	| bij iteratief algoritme stoppen wanneer ε<sub>a</sub> < ε<sub>s</sub>|

Bronnen van fouten:

1. Idealisatie fouten
	* Theory en/of model formulering
	* Data-onzekerheid
	* Onjuiste discretisering
2. Input Errors (blunders!)
3. Process Errors (algoritme-afhankelijk)
	* Blunders, logische fouten
	* Truncatie
4. Manipulatiefouten
	* affronding
	* cancellatie
5. Output Errors
	* Blunders
	* Intepretatie
	
Voornaamste errorsoorten:

* Round-off errors: door het kleine aantal significante digits dat een computer kan bijhouden
* Truncation errors: door de benadering van numerische methode

### Round-off error
**Round-off**: gevolgens van computervoorstelling van getallen
Een gelimiteerd range van hoeveelheden kan voorgesteld worden, omdat er maar een eindig aantal getallen zijn in mantissa en exponent. Hetinterval tussen voorstelbare getallen is niet uniform. De **Machine Epsilon** is het kleinste getal ε zodat 1 + ε > 1

Problemen wanneer:

1. kleine getallen opgeteld/afgetrokken worden bij grote getallen. Opl: eerst alle kleine optellen/aftrekken.
2. getallen die net verschillend zijn afgetrokken worden (**subtractive cancellation**). Opl: proberen formule herschrijven
3. Beide tesamen! :0

De round-off fout accumuleert over berekeningen.
De enige echte manier om het te minimaliseren is door meer significante cijfers toe te voegen aan de hardware.

### Truncation error
Fout bij door benaderingen.



Totale fout = round-off error + truncation error

Des te kleiner de stapgrootte h, des te kleine de truncation error.
Des te groter de stapgrootte h, des te kleiner de round-off erorr (omdat er minder berekend wordt).


## Numerical Error Propagation

*	Overgenomen: Phenomenon modeling (nooit echt bekeken)
*	Rounding: gelimiteerd aantal digits
*	Truncation: gelimiteerde termen
*	Kleine errors in rekentools

### Number Representation

Errors:

* Overflow
* Underflow
* Decimal to Binary

IEEE 754 standaard voor FP

### Error Representation and Propagation
3 manieren om fouten te berekenen:

* Absolute fout
* Relatieve fout
* Reading fout

x = ~x + Δx, 
y = ~y + Δy

z = x + y = (~x + ~y) + Δz met Δz <= Δx + Δy

z = x * y = ~x * ~y + Δz met Δz <= Δx* ~y+ Δy*~x

z = x/y = (~x + Δx)/(~y + Δy) = ~x/~y * (1+Δx/~x)/(~y + Δy/~y), met Δz <= ~yΔx + ~x+Δy/(~y)^2

Voor een functie geldt dit ook, vergeet niet om kettingregel toe te passen!

## Integers
### Numeric Encodings
Bv: unsigned, 2's c, ..

Bij 2's c staat de meest significante bit voor de signbit.
Het aantal getallen mogelijk bij 2c is dus de helft kleiner dan die van unsigned.

Belangrijk bij 2c: er is altijd 1 meer negatief getal dan positief. Biij 2-bit 2c: 00 = 0, 01 = 1, 10 = -1, 11 = -1

Sign extension: bij unsigned, gewoon 0 toevoegen. Bij 2c, het sign bit toevoegen.

Zijn er redenen voor unsigned? 

* wanneer modulaire rekenending(multiprecisie)
* wanneer er een extra bit range nodig is

## Floating Point number representation
Floating point is om een breuk binair voor te stellen.
Doordat het binair is kunnen enkel getallen van de form x/2^n voorgesteld worden.

Bv: 1/3 = 0.0101010101[01]..

Encoding: signbit | exponent E | breuk M

E = Exponent - Bias

M heeft altijd impliciet een leading 1, leidt tot een 'gratis' bit.

## Analysis of Algorithms

Een algoritme is een stap-per-stap procedure om een probleem op te lossen binnen een eindige tijdsperiode.

Running time, de tijd die nodig is voor een algoritme. Deze groeit meestal afhankelijk van de hoeveelheid data moet verwerkt worden. De gemiddelde run time is moeilijk te bepalen, dus wordt van de worst case running time vanuit gegaan.

Men kan experimenteel een algoritme nagaan door deze met verschillende hoeveelheden data te meten en dit te plotten. Nadelen: het is nodig om het algoritme te implementeren, wat moeilijk kan zijn, resultaten zijn mogelijk niet generaliseerbaar en om twee algoritmes te vergelijken moeten altijd zelfde soft- en hardware gebruikt worden.

Oplossing: theoretische analyse! Karakteriseer een running time als een functie van de inputsize n. Hierbij hebben we oneindig RAM, maar kost elke geheugencall tijd.

Basiscomputaties door het algoritme noemen we een primitieve operatie. Deze zijn grotendeels onafhankelijk van de programmeertaal. Door pseudocode te bekijken, kunnen we het maximaal aantal primitieve operaties van een algoritme bepalen.

Vervolgens definieren we a als de snelste operatie, b als de traagste. De reele snelheid ligt tussen deze twee. De functie is bounded door twee andere functies.

Gegeven f(n) en g(n), kunnen we zeggen dat f(n) O(g(n)) indien er een c en n<sub>0</sub> is zodat:
f(n) <= cg(n) voor n >= n<sub>0</sub>

We kunnen big-Oh gebruiken om functies hun groeirate te bepalen en volgens te rangschikken.

Regels:

1. Als f(n) van orde d is, dan is f(n) O(n^d)
	1. drop lagere ordere termen
	2. drop constante factoren
2. Gebruik de kleinst mogelijke klasse van functies (niet n^2 gebruiken wanneer n volstaat)
3. Gebruik de simpelse uitdrukking van de klasse (niet 3n maar n)

