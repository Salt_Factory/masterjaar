

### Inleiding

https://www.youtube.com/watch?v=lvTqbM5Dq4Q -> Video over quantumcomputers die Desamblanx aanraadde.

* COMSEC = communicatie security, ofwel hoe veilig de transmit van communicatie verloopt.
* COMPUSEC = computer security, ofwel hoe veilig data opgeslagen staat. 

Beveiliging vormt altijd een slingerbewegen tussen COMSEC en COMPUSEC.
COMSEC:

* SSL/TLS: Transport Layer Security/ voorganger Secure Sockets Layer
* VOIP: Voice Over IP (via IPsec, zie verder)
* VPN: Virtual Private Network
* NFC: Near field communication
* WEP/WPA/WPA2: Wired Equivalent Privacy / Wi-fi Protected Access
* GSM/3G/4G

**Basisprobleem: het internet is niet ontworpen om veilig te zijn!**

COMPUSEC:

* Data veilig bewaren
	- harde schrijven encrypteren
	- geencrypteerde databanken
* Programma's veilig uitvoeren
	- TXT: Trusted Execution Technology (Intel)
	- TPM: Trusted Platform Module (breder)
* Authenticatie
	- eID
	- Kerberos
* Toegangscontrole
	- paswoorden
	- biometrische toegangscontrolen
	
Het grootste gevaar: aanvallen van binnenuit!

# Samenvatting Tanenbaum Chapter 8
Vroeger was iedereen gelukkig op het internet, nu zijn er allerlei securityproblemen.

Er zijn vier soorten problemen:

* Geheimhouding: enkel geauthoriseerde gebruikers mogen informatie lezen
* Authenticatie: met wie ben ik aan het praten alvorens ik sensitieve dingen deel?
* *nonrepudiation*: hoe kan ik bewijzen dat een klant effectief een grote bestelling gemaakt heeft?
* Integriteit: het bericht dat ik ontvangen heb is misschien gewijzigd.

Security is een kwestie over alle lagen van de TCP-IP stack! Zowel fysiek als datalink als whatever zijn belangrijk.
Op fysieke laag na, zijn alle vormen van beveiliging cryptografie.

* Application Layer: authenticatie/nonrepudiation
* Transport Layer: end-to-end encryptie
* Network Layer: e.g. firewalls
* Link Layer: link encryption
* Physical Layer: wire tapping

Er zijn drie 'vormen' van cryptografie te onderscheiden:

* Symmetrisch
* Asymmetrisch
* niet-inverteerbaar, bijv hash (werkt maar in 1 richting) 

## 8.1 Cryptografie
* cipher = karakter per karakter of bit per bit transformatie, zonder te kijken naar de linguistische structuur van een bericht.
* code = een woord vervangen met een ander woord of symbool 

Code's worden niet meer gebruikt.

* De plaintext = het bericht om te encrypteren
* De output = ciphertext
* Passive intruder luistert enkel af
* Active intruder onderschept en wijzigd een bericht
* Cryptanalysis = breken van ciphersleutels
* Cryptologie = het verzinnen van ciphersleutels


![](./Afbeeldingen/symmetrisch_encryptiemodel.png)

* D = Decryptie met sleutel K
* E = Encryptie met sleutel K
* P = plaintext

Bij decryptie mag er geen informatie verloren gaan! Analoog met nulvector & eigenwaarde.

![](./Afbeeldingen/wiskundige_notatie_symm.png)

**Kerhkoff's principle: ALl algorithms must be public; only the keys are secret**

Het tegenovergestelde (security by obscurity) werkt nooit.

Verder zijn er twee soorten basisencryptiemethoden:

* substitution cipher
* transposition cipher

Informatica steunt altijd op simpele principes.
### 8.1.2 Substitution Cipher
Elke letter of groep van letters wordt vervangen door een andere letter of groep van letters, zoals bijvoorbeeld bij de Caesercode.
Ze behouden de volgorde van symbolen, maar verbergen ze.

Monoalfabetisch = letters omwisselen

### 8.1.3 Transposition Cipher 
Transpositie verandert de volgorde van de letters, maar verbergt ze niet.
Bij een code van 8 letters, zijn er 8! mogelijke transposities.

### 8.1.4 One-Time pads
Het maken van een onbreekbare cipher is gemakkelijk.
Kies eerst een willekeurige bitstring als sleutel. Zet vervolgens de plaintext om in bitstring.
Tot slot, XOR de twee strings, bit per bit.

In theorie is dit immuun voor veel soorten problemen, aangezien er geen informatie in de ciphertext zit die gebruikt kan worden (in tegenstelling tot letterfrequentie bij substitution).

**Gigantisch praktisch nadeel: zender & ontvanger moeten de sleutel kennen & sleutel moet even lang zijn als de boodschap!**
De enige manier om dit te omzeilen is *Quantum Cryptography*, wat in de praktijk veel te duur is.

One-time pad is het allerveiligst maar toch keigevaarlijk hierdoor.

### 8.1.5 Two Fundamental Cryptographic Principles
Twee belangrijke principes:

* Redundancy: alle geencodeerde berichten moet informatie bevatten die niet nodig zijn om het bericht te snappen. Dit voorkomt dat active intruders willekeurige junk kunnen sturen die als een valid bericht ontvangen worden. De keerzijde is wel dat het gemakkelijker wordt voor cryptanalysten om te encodering te breken, aangezien deze kunnen zien of het bericht dat ze genereren een valid formaat bevat. Een goed voorbeeld van redundancy is een CRC of cryptografische hash, aangezien deze minder voorspelbare resultaten geven dan padding met nullen. Een CRC geldt niet als authenticatie (voor versheid), aangezien een aanvaller deze zelf kan genereren.
* Freshness: alle berichten moeten geverifieerd kunnen worden op *versheid*, oftewel dat het een recent bericht is. Dit om replay-attacks tegen te gaan. Een voorbeeld hiervan is een timstamp, cookies of een lus in een s-box (simuleert geheugen).

Redundantie is ook mogelijk aan de hand van Hamming Codes. De **Hamming Afstand** tussent wee bitstrings is het aantal verschillende bits. Een Hammin code maakt van data-strings code strings door redundante bits toe te voegen zodat de minimale Hamming afstand maximaal wordt. Een voorbeeld hiervan is de pariteitsbit. In praktijk kan hamming 2 fouten detecteren & 1 fout corrigeren. 

Verder, is het ultieme doel van cryptografie om plaintext te vertalen naar **ruis**: deze bevat immers (ogenschijnlijk) geen informatie.

## 8.2 Symmetric-key algorithms

Moderne cryptografie gebruikt nog steeds transpositie en substitutie, maar met een andere nadruk. 
Het is de bedoeling om de algoritmes zo complex te maken dat zelfs wanneer een cryptanalyst tonnen data heeft, de code niet te kraken is.

Hierbij gebruiken we **block ciphers**.
Deze hebben als input een n-bit block aan plaintext, en zetten deze om aan de hand van een key in een n-bit block van ciphertext.

De basisoperaties permutatie en substitutie worden geimplementeerd als **P-box** en als **S-Box**.
![](./Afbeeldingen/basiselementen.png)

Een P-box (permutation) heeft een n-bit input, en levert een n-bit output. Hier is het Kerkhoff principe geldig: de aanvaller weet dat er permutatie gebruikt wordt, maar weet niet wat de sleutel is.

Een S-box (substitution) bestaat uit een decoder, een encoder en een P-box. 
In het bovenstaande voorbeeld heeft de S-box een 3-bits input, en een 3-bits output.

De voornaamste kracht van deze twee blokken wordt zichtbaar wanneer we ze casaderen in een hele serie aan boxes om een **product cipher** te vormen. In plaats van 1 grote S-box worden er 4 kleinere S-boxen gebruikt, omdat als een S-box 12 inputlijnen had, ze 2^12 (4096) draden zou hebbn in he midden.

### 8.2.1 DES--The Data Encryption Standard
![](./Afbeeldingen/des.png)

DES bevat 19 stages, een 56-bit key en zet 64 bits plaintext om in 64 bits ciphertext.
De eerste stage is een key-onafhankelijke transpositie, en de laatste stage is het omgekeerde van deze keyonafhankelijke transpositie. De voorlaatste stage verwisseld de meest linkse 32 bits met de meest rechtse 32 bits. 
Alle 16 andere stages zijn identiek maar worden geparametriseerd door andere delen van de sleutel. Decryptie is gewoon omgekeerde encryptie (wat nodig is voor een symmetrisch algoritme te zijn).

In zo'n stage zijn de operaties vrij simpel: het nieuwe linkergedeelte is gewoon het eerdere rechtergedeelte, en het nieuwe rechtergedeelte is een XOR van de linkse input en een functie van de rechterinput + de key voor de stage. Alle complexiteit volgt uit deze functie. Allereerst wordt een 48-bit getal gemaakt, E, door de 32-bit Ri-1 uit tebreiden. Vervolgens worden E en Ki geXORd. Deze input wordt dan in 8 groepen van 6 bits verdeeld, om vervolgens in verschillende S-boxen gevoed te worden. Elk van de 64 inputs van de S-box is gemapped op een 4-bit output, welke daarna door een P-box gaan. 

De stage-specifieke slutels worden bekomen door de sleutel in twee 28-bit units te delen, welke elk naar links geshift worden met een hoeveelheid bits afhankelijk van de iteratie. 

**Whitening** is het sterker maken van DES door eerst een willekeure 64-bit key te XOR'en met de plaintext en vervolgens de ciphertext te XOR'en met een tweede key.

Probleem van DES: key was te kort!

Oplossing: **Triple DES**.

![](./Afbeeldingen/DESDESDES.png)

Hierbij wordt er drie keer DES (twee encrypties; 1 decryptie) uitgevoerd met twee sleutels.
Waarom twee encrypties en 1 decryptie, in plaats van 3 encrypties?
3 keys zou voor teveel overhead zorgen, en 112 bits is onnodig voor routine toepassingen.
EDE behoudt de achterwaardse compatibiliteit van single-key DES systemen. Wanneer we single-key willen werken met EDE, stellen we gewoon K1=K2, waardoor de netto-operatie voor 1 encryptie zorgt.

### 8.2.2 AES--The Advanced Encryption Standard
Gebruikt de encryptie techniek genaamd Rijndael.

1. Het is symmetrisch
2. Het volledige design is publiek
3. Keylengtes van 128, 192 en 256 zijn ondersteund (al in praktijk enkel 128 en 256)
4. Zowel mogelijk als software als hardware-implementatie

Rijndael ondersteunt keylengtes en blocksizes tussen 128 bits en 256 bits, met stappen van 32 bits. Deze twee lengtes mogen onafhankelijk van elkaar gekozen worden.
Bij een 128 key zijn er 3*40^38 keys mogelijk, wat bij een machine van 1 biljoen processoren met 1 key per picosecone ng steeds 10^10 jaar zou duren.

Rijndael is gebaseerd op Galoisvelden, waardoor het sommige wiskundig bewijsbare eigenschappen bevat.
Het maakt gebruik van substitutie en permutaties, en ook van meerdere rondes (de hoeveelheid rondes hngt af van key size en blocksize, en ligt tussen 10 en 14). In tegenstelling tot bij DES, bevatten gaan alle operaties bij Rijndael over volledige bytes.

Het decrypteren van een met AES geencrypteerde boodschap, is gewoon een omgekeerde encrypte met dezelfde sleutel. 

### 8.2.3 Cipher Modes
Desondanks de complexiteit, is AES/DES/andere block cipher een monoalphabetische substitutie-cipher. Wanneer de zelfde plaintext erin gaat, komt altijd dezelfde ciphertext eruit. Dit zorgt voor een zwakte.



Bijvoorbeeld, bij **Electronic Code Book mode**. Hierbij wordt een string opgebroken in 8 byte blocks, welke een voor een geencryteerd worden met dezelfde key. Door de volgorde te veranderen waarin de ciphertext verstuurd wordt, kan de boodschap verandert worden. Bij ECB is er dus afhankelijkheid tussen blokken (wegens ontbreken feedback).

Cipher modes:

* **ECB**
* **CBC**
* **CFM**
* **SCM**
* **Stream cipher**
* **Counter Mode**

Om dit tegen te gaan kan gebruik gemaakt worden van **Cipher Block Chaining**. Elke plaintext block wordt hierbij geXOR'd met de vorige ciphertext block. Zo wordt de volgorde waarin de blokken aankomen wel van belang.
De eerste plaintext block wordt geXOR'd met een willekeurig gekozen **initialisativector**.
Een bijkomend voordeel is dat dezelfde plaintext niet meer altijd dezelfde ciphertext bekomt.

![](./Afbeeldingen/cipherblockchaining.png)

Het nadeel hiervan is dat een volledige 64-bit block moet gearriveerd zijn alvorens het kan encrypteren. Bij interactieve terminals is dit niet bruikbaar. Hiervoor kunnen we **Cipher Feedback Mode** gebruiken, wat byte-per-byte encryptie is.

![](./Afbeeldingen/cipherfeedbackmode.png)

In het voorbeeld zijn byte 0 t.e.m. 9 al verstuurd. Bij het arriveren van plaintext 10, genereerd het shift register een 64-bit ciphertext. Hiervan wordt de meest linkse byte gebruikt om te XOR'en met P10, waarna het resultaat verstuurd kan worden.
Het shift register shift met een byte, en voegt C10 toe. Een patroon dat meermaals voorkomt zal telkens keer een andere ciphertext genereren. Ook hier is een **initialisatievector** nodig.
Het voornaamste probleem met deze techniek is dat wanneer er een bit corrupt wordt tijdens transmissie, alle volgende 8 bytes fout gedecodeerd zullen worden.

![](./Afbeeldingen/streamciphermode.png)

Wanneer er een 1-bit transmissiefout geen 8 bytes mag corrumperen, kan **Stream Cipher Mode** gebruikt worden.
Hierbij genereert een key samen met een initialisatievector een **keystream**, met welke de plaintext gan geXOR'd worden, zoals bij een one-time-pad.
Het is belangrijk dat een IV-key paar nooit herbruikt wordt, omdat dit altijd dezelfde stream genereert en hierdoor **keystream reuse attacks** mogelijk worden.


![](./Afbeeldingen/countermode.png)

Het probleem met al de vorige modes op ECB na is dat er geen random access mogelijk is. 
Hiervoor kan **counter mode** gebruikt worden. De plaintext bij counter mode wordt niet direct geencrypteerd, maar een initialisatievector plus een constante worden geencypteerd, waarna het resultaat geXOR'd wordt met de plaintext.

De zwakte van counter mode is dat wanneer een key herbruikt wordt, en een aanvaller alle ciphertext van beide runs bekomd, enkel een XOR van de twee xiphertext volstaat om de XOR van de plaintext te bekomen.

![](./Afbeeldingen/symmetrischekeyalgoritmes.png)

## 8.3 Public-Key Algoritmes
Historisch gezien zit de grootste zwakte van symmetrische algoritmes bij het distribueren van de sleutel.
Diffie en Hellman (wat een naam) bedachten hiervoor assymetrische algoritmes.

Deze had drie vereisten:

1. D(E(P)) = P.
2. Het moet moeilijk zijn om D van E af te leiden
3. E kan niet gebroken worden door een chosen plaintext attack.

Als aan deze vereisten voldaan is, kan de encryptie sleutel publiek gemaakt worden.

Om het te gebruiken, bedenkt persoon A een algoritme E, en een algoritme D. De key van E maakt hij/zij publiek, en de key van D houdt hij/zij voor zichzelf. Persoon B doet hetzelfde.
A berekent Eb(P) (encryptie met Bob's sleutel) en stuurt het naar Bob. Bob decrypteerd de boodschap met zijn geheime key.

### 8.3.1 RSA
Het enige probleem is het vinden van een algoritme dat voldoet aan de drie vereisten.
Een algoritme dat hierbij gevonden is, heet RSA en is enorm sterk.
Het voornaamste nadeel is dat er minstens 1024 bits nodig zijn als sleutel om veilig te zijn, tegenover 128 bits bij symmetrische algoritmes.

De methode werkt, kort, als volgt:

1. Kies twee grote priemgetallen p & q (meestal van 1024 bits).
2. 2. Bereken n = p x q en z = (p - 1) x (q - 1).
3. Kies een getal dat relatief priem is tot z en noem het d.
4. Vind e zodat e x d  = 1 mod z.

Om vervolgens een bericht P te encrypteren, berekenen we C = P^e (mod n). Om het te decrypteren, bereken P = C^d (mod n).
Om te encrpteren zijn e en n nodig (public key) en om te decrypteren zijn d en n nodig (private key). 

Een ander voorbeeld van public key algoritme is het **knapsack algorithm**. 

## 8.4 Digital Signatures
Belangrijk om digitale documenten te handtekenen.
Er zijn drie voorwaarden om bruikbaar te zijn:

1. De ontvanger kan de beweerde identiteit van de zender verifieren.
2. De zender kan niet later het bericht wijzigen.
3. De ontvanger kan onmogelijk het bericht zelf gemaakt hebben.

### 8.4.1 Symmetric-Key Signatures
Een mogelijke manier is Big Brother (BB), een systeem waarbij er een centrale authoriteit is die alles kent en wie iedereen vertrouwd. Elke user heeft een secret key die enkel hij-/zijzelf en BB kent.
Alice encrypteerd een berich voor Bob met haar key, Ka, en voegt hier Bob's identiteit, een timestamp en een willekeurig getal aan toe. BB decrypteerd het bericht, en stuurt het door naar Bob samen met het getekende bericht Kbb(A, t, P). 
Enkel Alice kan het bericht verstuurd hebben, en timestamps voorkomen replay attacks.

![](./Afbeeldingen/bigbrother.png)

### 8.4.2 Public-Key Signatures
Een probleem met de symmetrische keys is dat iedereen BB moet vertrouwen, en dat BB alles mag lezen.
Zie figuur.

![](./Afbeeldingen/publickeysignatures.png)

Hier komen wel problemen bij. Bijvoorbeeld, wanneer een secret key gestolen wordt, is moeilijk te bewijzen dat een bericht *niet* van Alice zou komen.
Ook is het problematisch wanneer Alice van sleutel zou willen wijzigen. Plots kan een bericht dan niet meer gedecrypteerd worden door Alice's public key.

### 8.4.3 Message Digests
Bij handteken methodes zijn er vaak twee functies: authenticatie en geheimhouding.
In sommige gevallen is dezegeheimhouding niet gewensd.

In zo'n situaties kan een **message digest** gebruikt worden, wat een hash functie is.
Deze functie heeft vier eigenschappen:

1. Gegeven P, is het gemakkelijk MD(P) te berekenen.
2. Gegeven MD(P), is het onmogelijk P na te gaan.
3. Gegeven P, kan niemand P' vinden zodat MD(P') = MP(P) (kan bereikt worden door minstens 128 bits te gebruiken als hash).
4. Een verandering van 1 bit geeft een enorm verschillende output.

Een MD kan ook in public-key systemen werken.

Een voorbeeld van zo'n MD functie zijn **MD5** en **SHA-1**.

![](./Afbeeldingen/hash.png)

### 8.4.4 The Birthday Attack
Om een m-bit MD te ondermijnen zijn geen 2^m operaties nodig, maar 2^m/2 operaties.
Een 64-bit message kan gebroken worden door ongeveer 2^32 berichten te genereren, en te zoeken naar 2 met dezelfde MD.

## 8.5 Management Of Public Keys
Met public-key cryptografie kunnen mensen die geen sleutel delen wel veilig communiceren. Het maakt het ook makkelijker om berichten te handtekenen zonder nood aan een derde partij (zoals BB). Tot slot laten getekende MD's het mogelijk om de integriteit van ontvangen berichten na te gaan.

Het voornaamste probleem nu is dat Alice & Bob elkaar niet kennen, dus hoe komen ze aan elkaar's publieke sleutels?
De sleutel publiceren op een eigen websitis te gevoelig voor aanvallen.

### 8.5.1 Certificaten
We creeren een organisatie die public keys certifieert van mensen, bedrijven en andere. Zo'n organisatie heet een **CA (Certification Authority)**.

![](./Afbeeldingen/certificaat.png)

Een certificaat moet een public key binden aan de naam van een persoon (burgerlijk of gerechtspersoon, kan beide).

### 8.5.2 X.509
X.509 is een standaard voor certificaten.
Au fond is het een manier om certificaten te beschrijven.

![](./Afbeeldingen/x509.png)

### 8.5.3 Public Key Infrastructures
Een centrale CA is risky. Daarbovenstaande, welke organisatie kan een CA hosten?
Hiervoor is er een andere manier van cerficieren ontstaan, genaamd **PKI (Public Key Infrastructure)**.
Een PKI zorgt voor structuur tussen CA's, certificaten en directories. De top-level CA wordt de **root** genoemd, en deelt certificaten aan de second-level CA's, welke **RA (Regional Authority)** genoemd worden. Dit leidt tot een **Chain of Trust**.
In praktijk wordt er niet gebruik gemaakt van 1 root, maar van honderden roots. Op deze manier is er geen 1 enkele trusted authority.

![](./Afbeeldingen/pki.png)

Een bijkomend probleem is de locatie van de opslag van de certificaten. Een mogelijkheid is om elke gebruiker zijn eigen certificaten bij te houden, maar dit is ongemakkelijk. 
Een andere mogelijkheid is een **directory**, wat een dedicated opslag is van certificaten.

Een laatste probleem is dat van herroeping (**revocation**). 
De gever van een certificaat mag beslissen om het in te trekken. 
Hiervoor geven CA's regelmatig een **CRL (Certificate Revocation List)**, wat alle serienummers van teruggeroepen certificaten bevat. Spijtig genoeg moet een user dan altijd nagaan of een certificaat niet ingetrokken is.
Bijkomend, waar moeten CRL's opgeslagen worden? CA's kunnen CRLs periodisch pushen en directories de ingetrokken certificaten laten verwijderen.


## 8.6 Communication Security
### 8.6.1 IPsec
Een veelgestelde vraag was vroeger: "Waar moet beveiliging komen?" 
Dit kon ofwel bij de users, ofwel in de netwerklaag. 

In IPsec moet een gebruiker altijd gebruik maken van encryptie, maar is het toegestaan om een null algoritme te gebruiken. 
Het volledige IPsec design is een framework dat meerdere diensten, algoritmes en granulariteiten toelaat. De diensten kunnen a la carte gekozen worden.

IPsec is algoritme onafhankelijk, zodat wanneer een huidig algoritme gebrken wordt, dit in de toekomst vervangen kan worden. 
De granulariteit is schaalbaar zodat gekozen kan worden in welke grootte encryptie wordt toegepast: 1 TCP pakket, alle verkeer tussen 2 hosts of alle verkeer tussen een heel netwerk.

IPsec bevindt zich in de IP laag, maar is wel **Connection oriented** (logisch, want een connectie moet kunnen opgezet worden). Een connectie in IPsec context is een **SA (security association)**.

IPsec kent een **transport mode** en een **tunnel mode**. 
In transport mode is wordt de header vlak achter de IP header geplaatst, en wordt de IP header aangepast om aan te duiden dat een IPsec header volgt.

In tunnel mode wordt het volledige IP pakket geencapsuleerd in een ieuw IP pakket met een compleet nieuwe header. Dit is handig wanneer de tunnel eindigd in een andere locatie dan aan de eindbestemming. 

De eerst header is de **AH (Authentication Header)**. Deze verzorgd integriteitschecking en antireplay beveiliging, maar geen geheimhouding. 

![](./Afbeeldingen/authheader.png)

### 8.6.2 Firewalls
Een firewall is een digitale gracht rond het kasteel. Alle transport moet over 1 brug gaan, waar het nagekeken kan worden. 

Het vertrekpunt van security is om een perimeter rond het systeem aan te leggen, zonder dat er ergens een zwakke schakel is. Dit dient als eerste stap om een inbraak te compliceren, maar kan het nooit volledig voorkomen.

![](./Afbeeldingen/vuurmuur.png)


### 8.6.3 Virtual Private Networks

![](./Afbeeldingen/vpn.png)

Het huren van een telefoonlijn kost bakken vol geld, maar leidt wel tot goeie beveiliging.
Een VPN probeert dit na te bootsen, zonder de hoge kost.

Alle poorten van de router gaan dicht, tot een pc zich virtueel aanmeldt. Het voordeel hiervan is tegelijker tijd het nadeel. Beveiliging is leuk, maar als het teveel moeite kost is dit minder leuk.

### 8.6.4 Wireless Security
Omdat veel verkeer draadloos gebeurd, komt hier geen firewall aan te pas en kan dit zo uit de lucht geplukt worden. 

De **802.11** standaard beschrijft een datalink beveiligingsprotocol genaamd **WEP (Wired Equivalent Privacy)**.
Deze heeft als doel om WLAN even veilig te maken als LAN.

Elk station heeft een geheime sleutel dat het eeld met het basisstation. Ofwel de gebruiker ofwel het basisstation kiezen een willekeurige key en versturen het naar de andere, geencrypteerd met de ander zijn public key.
WEP gebruikt hiervoor een streamcipher gebaseerd op **RC4**. 

Probleem hierbij is dat veel installaties dezelfde gedeelde sleutel gebruiken voor alle users, waardoor elke user elkaar's verkeer kan ontcijferen. Verder kan WEP nog steeds aangevallen worden. Wanneer veel pakketjes onderschept kunnen worden met dezelfde IV waarde, kan zo de XOR achterhaald worden. Ook zijn de IV's maar 24 bits, en is na 2^24 ste pakketes herbruik nodig. De IV is de achilleshiel van het systeem.

**WPA** werkt anders; iedereen heeft zelfde PSK, maar verkrijgt een andere sessionkey. Op deze manier is er 1 wachtwoord, maar kunnen gebruikers elkaar niet afluisteren. 
4-way authenticatie vermijdt hier replay attacks door session key op te bouwen uit delen. 

**Bluetooth Security** mag ook niet vergeten worden. Bij bijvoorbeeld een onderschept bluetooth-toetsenbord kunnen alle toetsaanslagen uitgelezen worden. 
Bluetooth heeft drie beveiligingsmodes, gaande van geen security tot volledige data-encryptie en integriteitscontrole.

De beveiliging komt in meerdere lagen. In de fysieke laag geeft frequentiehopping een beetje veiligheid, maar aangezien de frequentiehoppingsequentie moet verstuurd worden, is deze sequentie geen geheim.
Wanneer een nieuwe slavedevice zich aanmeldt, zijn er twee mogelijkheden:

* Ze delen een geheime code die op voorhand werd gemaakt, en soms wordt gehardcode.
* Het ene device heeft een gehardcode key, en de user moet deze ingeven in het andere device

Deze gedeelde sleutels heten **passkeys**.
Beide devices gaan na of de andere device de passkey kent. Vervolgens kiezen we een 128-bit sessiekey, met eventueel een aantal publieke bits.
De encryptie maakt gebruik van stream cipher E0; de integriteitscontrole maakt gebruik van **SAFER+**. 
Beide zijn symmetrisch.

## 8.7 Authentication Protocols
**Authenticatie** is de techniek waarbij een process verifieerd dat zijn communicatiepartner de juiste persoon is, en niet een bedrieger. Het doel is dus eigenlijk om een sessiesleutel te kunnen verkrijgen, om zo veilig te kunnen communiceren

**Authorizatie != Authenticatie!**
Authorizatie is wat een proces mag doen, authenticatie is nagaan of het wel het juiste proces is.

Au fond werken alle authorizatiemodellen hetzelfde:
Alice stuurt een bericht naar Bob of naar een **KDC (Key Distirbution Center)**, waarna er een uitwisseling van berichten volgt.
Trudy kan elk bericht onderscheppen, aanpassen of replayen. 
Van zodra het protocol klaar is, weten A en B met zekerheid dat ze met elkaar praten. In de meeste gevallen gaan ze hierbij een **secret session key** hebben opgesteld. Het nut van het telkens willekeurig kiezen van een sessiesleutel is om het gebruik van dezelfde sleutel te limiteren, zodat r minder ciphertext is en zodat er zo min mogelijk schade is wanneer de sleutel lekt.
 In praktijk wordt voornamelijk symmetrisch geencrypteerd.
 
 

### 8.7.1 Authentication Based on a Shared Secret Key
A & B delen een sleutel: K<sub>BA</sub>.
De ene stuurt een willekeurig getal, waarop de andere dit transformeert en terugstuurt.
Dit is een **challenge-response** protocol. 

* A,B zijn Alice & Bob
* R<sub>i</sub>'s zijn de challenges, met i de challenger
* K<sub>i</sub> zijn de sleutels, met i de eigenaar
* K<sub>s</sub> is de sessiesleutel

![](./Afbeeldingen/twowayauth.png)

1. Alice stuurt haar identiteit naar Bob
2. Bob stuurt een **nonce** terug, dit is een willekeurig getal dat maar een keer gebruikt wordt in het protocol
3. Alice encrypteerd het met de gedeelde sleutel en stuurt het terug op. Nu weet Bob dat het met Alice praat, maar Alice nog niet dat het met Bob praat.
4. Alice stuurt haar eigen nonce.
5. Bob stuurt het geencrypteerd terug.

Deze vjif berichten zijn te herleiden naar drie. 

![](./Afbeeldingen/twowayauthshort.png)

Deze implementatie bevat wel een groot probleem: de **reflection attack**.

![](./Afbeeldingen/reflection.png)

### 8.7.2 Establishing a Shared Key: The Diffie-Hellman Key Exchange.
Wat als er geen gedeelde sleutel is? Hiervoor kan de **Diffie-Hellman key exchange** gebruikt worden.
Ze kiezen samen twee grote getallen, n en g, waarbij n een priemgetal is en (n-1)/2 ook een priemgetal is.

![](./Afbeeldingen/dhke.png)

Het enige wat niet mogelijk is met deze techniek is authenticatie. Daarom is het vatbaar voor man-in-the-middle attakcs.

![](./Afbeeldingen/bucketbrigade.png)

### 8.7.3 Authentication Using a Key Distribution Center
Een andere mogelijkheid is een **trusted Key Distribution Center (KDC)**. 
Iedere gebruiker heeft hier een gedeelde sleutel met een KDC. Alle authenticatie en sessiesleutel management wordt nu door de KDC gedaan.

![](./Afbeeldingen/kdc.png)

Hier is een replay attack door Trudy wel nog mogelijk.
Meerdee oplossingen bestaan: teimstamps, nonces (die maar in 1 bericht mogen voorkomen), of een **multiway challenge-response protocol**.

Een voordeel is wel dat een afluisteraar niet kan zien met wie er wordt gebabbeld.

### Email:

* PGP
* PEM
* S/MIME

### Web Security:

**SSL/TLS**: communicatie tussen (virtuele sockets)

1. Client-server protocol voor uitwisseling parameters
2. Client-server authenticatie
3. Geencrypteerde communicatie
4. Bescherming van data integriteit
5. Cipher: vooral AES



# Examen- en herhalingsvraagjes
* Bespreek DES/AES/SA
* Hamming-afstand
* Wat is verband & verschil tss CRC & Hamming -> doel is anders: foutdetectie vs encryptie
* Wat zijn blockciphermodes en welke zijn gekend?
	* ECB: gewoon niets
	* CTR: counter laten doorlopen, haalbaar parellellisme, ruis maar 1 fout maar halve files moeilijk
	* CBC: heeft IV nodig & werkt moeilijk parallel
	* CFB: CBCmet paar bits ipv volledig, even sterk, makkelijker parallel
	* stream cipher: sleutel niet om boodschap te encrypteren maar om keystream te genereren, IV nodig & ruis is nefast (kan wel asynchroon)
	
* IPsec, firewall, VPN, wifi, Authenticatie, Web Sec
	* Welke bouwstenen, welke algoritmes, ...
* Leg uit: SSL/TSL
	