# Begrippenlijst hardware

| begrip | uitleg |
| --------- | -----------|
|DRAC (Dell)/ILO(HP) | virtuele powerknop + analytics, actief ook wanneer de server offline is (maar er wel nog stroom is.) Soms enkel lokaal, hosten soms ook een webserver die dan remote te bereiken is, om zo de analytics uit te lezen & toegang te geven tot de powerknop.|
|UPS | 	Uninterruptible Power Supply, een extra PSU die werkt wanneer de stroom uitvalt, zodat de servers (tijdelijk) blijven draaien.|
|Mainenance HW | Ding waarbij de schijf zelf gaat signaleren dat de hij vervangen moet worden. Kan ook als SW, maar dan gebeurt installatie van die SW door supplier (en das meestal duur!). |
| VMWare| Software die toelat tot virtualisatie. <br> + meerdere 'servers' per machine <br> + meer stabiliteit (elke virtual machine kan software apart draaien ipv tesamen in 1 vM) <br> - 1 machine, bij falen zijn alle vm's kapot (opl: 2de mchine met gedeelde VM) <br> - als machine 1 crasht, kan machine 2 de vm's niet opstarten wanneer de data op 1 staat |
| darkfiber | glasvezel zonder signaal, dus ongebruikt |
| BELNET | Belgische netwerkbackbone, op federaal niveau | 
|   SAN | Storage Area Network. Bevat twee ctrls, twee switches (elk crossconnected), elk eigen PSU, switches onderling ook verbonden zodat ze IP delen tijdens startmodus. SAN heeft een eigen filesysteem (moet geformatteerd worden) en werkt met datablokken.|
| NAS | network attached storage, werkt als filesharing (wordt gezien als 'lokale' schijf) |
| PIO | Programmed Input/Output, CPU stuurt het ophalen uit het geheugen volledig aan. |
| DMA | Direct Memory Access, 'apart' memory-unit op CPU om de load op de CPU te verminderen. |
| CISC | Complex Instruction Set, instructieset met veel instructies, vaak groot, om vanalles te doen. |
| RISC | Reduced Instruction Set, instructieset met weinig instructies. |
| Itanium | Serverprocessor, deelt code op in stream om expliciet parallellisme te bereiken.  <br> + betere benutting execution units <br> + minder overhead (de compiler parallelliseert, niet de HW) <br> - meer registers nodig om alle variabelen te stockeren <br> - compiler pokkeduur!|
| Predication (bij Itanium)| Beide paden van een conditie uitrekenen, de juiste bijhouden. Mogelijk dankzij de hoeveelheid execution units. |
| Speculation (bij Itanium)| verplaatst memoryoproepen naar voor in de code, zodat de memory klaar is wanneer het nodig is. Vooral handig wanneer data van HDD of network komt. | 
| RAS | Features van Itanium. <br> * Reliability: CRC op data<br> * Availability: software error gaat zo goed mogelijk afgehandeld worden (niet droog bluesceen), reliability lost softerrors op en hard errors worden vermeden door kwaliteit. <br> * Serviceability: on-line replacement van kaarten, cpu, memory, ...|
|OLR| on-line replacement | vervangen van HW tijdens runtime zonder onderbreking. | 
|OOO| out-of-order execution, waarbij logica in de CPU gaat proberen om de instructies ipv sequentieel uit te voeren, te parallelliseren. Dit kan enkel wanneer ze onafhankelijk zijn van elkaar. |
| CU | Computing Unit. ALU voor integer, FPU voor floating point. | 
| FSB | Front Side Bus, verzorgde interconnectie tussen processoren en chipset. | 
|DIB | Dual Independent Buses, vervanger van FSB.  CPU's delen een bus per 2. Probleem: wat als meest linkse CPU data nodig heeft van meest rechtse? Snoop filter nodig!|
| DHSI | Dedicated High Speed Interconnects, vervanger van DIB. Elke processor krijgt een eigen bus naar de chipset. |
|QPI | QuickPath Interconnect, elke processor verbonden met elke processor en een chipset. Voornaamste technologie.| 
| Hyper-threading | Technologie die toelaat dat 1 processor 2 softwarethreads tegelijkertijd kan uitvoeren. |
| Execute disable bit | Bit die voorkomt dat CPU kan executen. Bijvoorbeeld, bij het inlezen van data wordt deze altijd opgezet zodat niet per ongelijk verkeerde files uitgevoerd worden. |
|IDE| Integrated Drive Electronics, voorganger van ATA |
|ATA | AT Attachment | 
| MR | magnetoresistive head, leeskop waarbij de weerstand van het materiaal in de kop verandert |
| GMT | giant magnetoresistive head, zelfde als MR maar kleiner en beter. |
| SMART | Self-monitoring, Analysis and Reporting Technology 	is een systeem dat zich in schijven bevindt en hardware failures kan spotten. |
| Interleaving | Niet sequentieel lezen/schrijven naar een disk, maar telkens een bepaald aantal zones skippen. bv, 2:1 |SSD | Solid State Driver |
|MLC | Multi layer Cell, 2 bits per datacel |
| TLC | Triple level cell, 3 bits per datacel|
| GC | Garbage Collection, het proces van het verplaatsen van bestaanda data naar andere locaties, zodat de omringende invalid data verwijderd kan worden bij SSD's.|
| OS Awareness | Bij HDD's kan het OS aanvragen dat nieuwe data geschreven wordt op de locatie van oude data.|
|Drive Awareness | Bij SSD's gaat dit niet, en moet de SSD dit zelf allemaal regelen. |
| TRIM | Commando om een SSD te vertellen welke data niet meer valid is | 
| wear leveling | write cycles rond de chip van de SSD bewegen zodat alle cellen gelijk eroderen |
| write optimisation | Datawrites tijdelijk opslaan, zodat ze in grote delen tesamen kunnen geschreven worden. |
| NVMe| Non-Volatile Memory Express, gebaseerd op PCIe | 
| QIC | quarter-inch cartridge |
| DAT | Digital Audio Tape | 
| AIT | Advanced Intelligent Tape | 
| DLT | Digital Linear Tape   |
| ADR | Advanced Digital Recording | 
|LTO | Linear Tape Open |
| ROM | Read Only Memory|
|PROM | Programmable ROM|
|EPROM | Erasable PROM|
|EEPROM| Electrically Erasable PROM |  
|RAM | Random Access Memory |
|SRAM | Static RAM|
|DRAM | Dynamic RAM|
| DDR | Double Data Rate |
| RDRAM | Rambank DRAM|
|DAS|Direct Attached Storage |


| NAS | SAN |
|--------|---------|
|files | blocks |
| werkt alsof netwerkschijf | werkt als device |
| kan niet remote geformatteerd worden | kan wel remote geformatteerd worden|
| user kan niet zomaar alles doen | user kan doen wat hij wil met de blokken die hij heeft |
| filesystem aanwezig | filesystem zelf installeren (voordeel: meerdere mogelijk)|
