# Documenten Masterjaar

Beste vriendjes van het masterjaar,

In deze repo steken al mijn files voor de vakken van het masterjaar.
Dit zijn onder andere:
* Alle documenten vanop Toledo;
* Voor vakken met veel slides heb ik de slides samengezet in 1 groot document;
* Een bundeling van de examenvragen die ik heb kunnen vinden, per vak.
* Labocode;


Als ik ergens iets fout geschreven heb, documenten mis of als ik nog slides of examenvragen niet heb, aarzel niet om een bericht te sturen of om een merge request te maken.
Voor de examenvragen zitten de oorspronkelijke .ods (libreoffice) bestanden ook in de repo, dus kunnen jullie gemakkelijk toevoegen indien nodig.

