#!/usr/bin/csh
if ($#argv != 1) then
	echo "Ik kan maar 1 argument aan! :("
	exit 1
endif
set argument=$argv[1]
echo $argument
if (-f $argument) then
	echo "'Tis een bestandje!"
else if (-d $argument) then 
	echo "'Tis een foldertje!"
else  
	echo "'Tis een niks!"
endif
