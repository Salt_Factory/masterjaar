/*
 *      namei.c         met eventuele stub : om te kunnen testen 
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#define EXT extern
#include "hfs.h"


#ifdef STUB
short namei(char *cpn)
{
	static Inode	ipb;
	Inode	*ip;
	char	*cpl;
	short wdir = 0;

	printf(" diract: offset in dir : "); scanf("%hd%*c", &u.u_diract);
	printf(" parent inode nummer : "); scanf("%hd%*c", &u.u_pdir);
	printf(" inode nummer : "); scanf("%hd%*c", &wdir);
	if ( wdir != 0 )
	{
		ip = LeesInode(wdir, &ipb);
		if ( verbose && ip )
		{
			printf("%3d : ", wdir);
			DrukInode(ip);
		}
		u.u_dirent.d_ino = wdir;
	}
	else
	{
		ip = (struct inode *)NULL;
		u.u_dirent.d_ino = 0;
	}
	/* afsplitsen van de laatste component van de padnaam */
	cpl = strrchr(cpn, '/');
	if ( cpl == NULL ) 
		cpl = cpn;
	else
		cpl++;
	memset(u.u_dirent.d_naam, '\0', NAMELEN);
	strncpy(u.u_dirent.d_naam, cpl, NAMELEN);
	if ( verbose )
		printf("dirent %d %-7.7s   diract %d   pdir %d\n",
			u.u_dirent.d_ino, u.u_dirent.d_naam, u.u_diract, u.u_pdir); 
	u.u_error = 0;
	return wdir;
}
#else
short namei(char *cpn) //cpn is de volledige padnaam van de file
{

	char *filename;
	Inode werkinode;
	int padteller;
	int naamteller;

	filename = strrchr(cpn, '/');
	printf("namei bestand  %s \n", cpn);

	//nakijken of we beginnen aan padnaam
	if (cpn[0] == '/')
	{
		printf("Pad start niet met root\n");
		LeesInode(u.u_rdir, &werkinode);
		padteller = 1; //de / skippen sebiet
	}
	else
	{
		printf("Pad start niet met root\n");
		LeesInode(u.u_cdir, &werkinode);
		padteller = 0;
	}
	//zolang er nog padnaam overblijft
	naamteller = 0;
	while(cpn[padteller] != '\0')
	{
		//inlezen component van pad in u.u_dirent.dnaam

		memset(&u.u_dirent.d_naam,0,NAMELEN);
		while(cpn[padteller] != '/')
		{
			u.u_dirent.d_naam[naamteller] = cpn[padteller];
			printf("d_naam teller: %d waarde: %c\n",naamteller,u.u_dirent.d_naam[naamteller]);
			naamteller++;
			padteller++;
			if (cpn[padteller] == '\0') //break wanneer we aan het einde zijn
			{
				break;
			}
		}
		//klaar met inlezen van pad in u.u_dirent.dnaam, hoezee
		
		if((werkinode.i_mode&S_IFMT) == S_IFDIR){

		}
		//TODO: toegangspermissies nakijken!
		
		//padteller eentje verder zetten indien we naar de volgende component kunnen
		if(cpn[padteller] == '/') 
		{
			padteller++;
			naamteller = 0;
		}

	}


	return 1;

	
}
#endif
