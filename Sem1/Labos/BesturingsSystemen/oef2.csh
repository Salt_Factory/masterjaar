#!/usr/bin/csh
#Author: Simon Vandevelde

#nakijken of er 1 argument is
if ($#argv != 1) then
    echo "Minstens en maximaal 1 argument!"
    exit 1
endif

#er is een argument, nakijken of het argument wel klopt
set arg = $argv[1]
if ($arg !~ [0-9][0-9][0-9]) then
    echo "Het argument klopt niet!"
    exit 1
endif

#het argument klopt, nu paden zetten
set foldernaam = "oef2" #argument voor foldernaam zodat het makkelijk aanpasbaar is!
set spad = /student/e4/hfd/tmp/$foldernaam #te kopieren folder
set dpad = $cwd  #doelfolder, zijnde de huidige directory

#twee testpaden voor thuis
#set spad = "/home/saltfactory/Documents/Projectjes/$foldernaam" #te kopieren folder
#set dpad = $cwd  #doelfolder, zijnde de huidige directory

#nu kunnen we kopieren! Hoezee
cp -r $spad $dpad

#vervolgens moeten we overal de filenamen aanpassen
set folders = `ls $dpad/$foldernaam` #een lijst van alle files krijgen in deze folder

foreach folder ($folders) 
    if ( -d $dpad/$foldernaam/$folder) then #nakijken of het ook effectief een folder is en geen file
        set files = `ls $dpad/$foldernaam/$folder` #lijst van alle files in folder krijgen
        foreach file ($files)                      #voor iedere file de naam aanpassen
            set newfile = `echo $file | cut -c 1-5`$arg #de laatste 3 characters cutten en de nieuwe appenden
            mv $dpad/$foldernaam/$folder/$file $dpad/$foldernaam/$folder/$newfile
        
        end
    endif

end


