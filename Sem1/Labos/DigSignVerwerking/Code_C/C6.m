%Author: Simon Vandevelde
%6de orde FIR met lineaire fase

%gegeven waarden:
fcutoff = 250; %Hz
fs = 1000; %kHz

%berekende waarden:
Wn = fcutoff/(fs/2);

B = fir1(15, Wn, 'low')

freqz(B)