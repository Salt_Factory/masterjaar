%Author: Simon Vandevelde
%8ste orde Cauer

%gegeven waarden:
fcutoff = 300; %Hz
rimpel = 0.5; %dB
minatt = 50; %dB
fs = 4000; %kHz
m = [16 8 5];

%berekende waarden:
Wn = fcutoff/(fs/2);


%Bereken de z, p
[z,p] = ellip(8, rimpel, minatt,  Wn);

%Toon het pz plot, en frequentieverloop
%zplane(z,p);
%w = waitforbuttonpress;
%freqz(z,p);

for n = m
    z1 = round(z,n)
    p1 = round(p,n)
    titel = sprintf('%d digits', n);
    zplane(z1,p1);
    title(titel);
    w = waitforbuttonpress;
    freqz(z1,p1);
    title(titel);
    w = waitforbuttonpress;
    impz(z1,p1);
    title(titel);
    w = waitforbuttonpress;
    roots(p1)
end
