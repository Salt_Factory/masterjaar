%Author: Simon Vandevelde
%8ste orde Cauer, door 4 2e ordes achter elkaar te plaatsen

%gegeven waarden:
fcutoff = 300; %Hz
rimpel = 0.5; %dB
minatt = 50; %dB
fs = 4000; %kHz
m = [16 8 5];

%berekende waarden:
Wn = fcutoff/(fs/2);

%zeros en polen
[z,p, k] = ellip(8, rimpel, minatt,  Wn);

[SOS,G] = zp2sos(z,p,1)
fvtool(SOS);
w = waitforbuttonpress;
round(z,2);
round(p,2);

[SOS,G] = zp2sos(z,p,1)
fvtool(SOS)

