%Author: Simon Vandevelde
%2e orde recursieve HDL-filter

%gegeven waarden:
a0 = 1;
a1 = 0.77;
a2 = 0.36;
b0 = 0;
b1 = 1;
b2 = -1;

teller = [b0 b1 b2];
noemer = [a0 a1 a2];

zplane(teller,noemer);
w = waitforbuttonpress;
freqz(teller,noemer);