%Author: Simon Vandevelde
%3e orde recursieve HDL-filter

%gegeven waarden:
a0 = 105;
a1 = -213;
a2 = 155;
a3 = -39;
b0 = 1
b1 = 3;
b2 = 3;
b3 = 1;

teller = [b0 b1 b2 b3];
noemer = [a0 a1 a2 a3];

zplane(teller,noemer);
w = waitforbuttonpress;
freqz(teller,noemer);

%% zelf bilineair

fcutoff = 0.99999; %rad/s
fs = 2; %Hz

[teller,noemer] = butter(3,fcutoff, 'low','s'); %maken van tfunctie
%freqs(teller,noemer); %tonen in het s-domein

[teller2, noemer2] = bilinear(teller,noemer, fs); %omzetten naar z-domein
zplane(teller2,noemer2);
w = waitforbuttonpress;
freqz(teller2,noemer2);