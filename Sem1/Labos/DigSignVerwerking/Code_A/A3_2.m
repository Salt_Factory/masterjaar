%Author: Simon Vandevelde
%matlab file voor het onderzoeken van de invloed van sampling op het
%frequentiespectrum van een golvorm in het continue tijdsdomein

%Gegeven waarden:
a = 5
r = 8000
p = 1000

n = 64

%Berekende waarden:
fs1 = r                  %samplefrequentie van r
Ts1 = 1/fs1               %sampleperiode
t1 = 0:1/fs1:(Ts1*(n-1))  %dynamisch de 64 stappen berekenen
freq1 = (0:(n-1))*fs1/n

%Berekende waarden:
fs2 = r+12.5              %samplefrequentie van r+12.5
Ts2 = 1/fs2               %sampleperiode
t2 = 0:1/fs2:(Ts2*(n-1))  %dynamisch de 64 stappen berekenen
freq2 = (0:(n-1))*fs2/n

%Berekende waarden:
fs3 = r+37.5              %samplefrequentie van r+37.5
Ts3 = 1/fs3               %sampleperiode
t3 = 0:1/fs3:(Ts3*(n-1))  %dynamisch de 64 stappen berekenen
freq3 = (0:(n-1))*fs3/n

%Berekende waarden:
fs4 = r+62.5              %samplefrequentie van r+62.5
Ts4 = 1/fs4               %sampleperiode
t4 = 0:1/fs4:(Ts4*(n-1))  %dynamisch de 64 stappen berekenen
freq4 = (0:(n-1))*fs4/n




%golfvorm:
x1 = a*cos(2*pi*p*t1)
x2 = a*cos(2*pi*p*t2)
x3 = a*cos(2*pi*p*t3)
x4 = a*cos(2*pi*p*t4)


subplot(4,1,1)
stem(freq1,abs(fft(x1)))

subplot(4,1,2)
stem(freq1,abs(fft(x2)))

subplot(4,1,3)
stem(freq1,abs(fft(x3)))

subplot(4,1,4)
stem(freq1,abs(fft(x4)))




