%Author: Simon Vandevelde

%A2: Convolutie
%matlab file voor het simuleren van de functionaliteit van convolutie

%Gegevens:

%de verschillende gesamplede functies
%f = [ 0 0 0 1 1 1 1 1 1 ]       
%f = [ 0 0 0 1 0 0 0 0 0 ]
%f = [ 0 0 0 1 0 0 0 1 0 0 0 ]
%f = [ 0 0 0 1 1 1 1 1 1 0 0 0 ]

%extra lange ingangsfunctie voor de laatste vraag van A2
f = [ 0 0 0 0 0 1 1 1 -1 1 0 0 0 0 0 0 -1 -1 -1 1 -1 0 0 0 0 0 ] 

%de verschillende coefficientfuncties
%c = [0.5 0.25 0.125 ]           
%c = [1/3 1/3 1/3]

%c = [ 0 1 1 1 -1 1 0 ] %coefficienten zijn kopie van ingangsfunctie
c = [ 0 1 -1 1 1 1 0 ] %coefficienten zijn gespiegelde kopie


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%einde gegevens, nu code

%we gaan de lengte van de x-as dynamisch berekenen
%om zo gemakkelijk te kunnen wisselen van functies en coefficientfuncties
%onnodig is onnodig, maar ik kan geen size opvragen zonder een extra
%variabele erbij
[onnodig, flengte] = size(f)
[onnodig, clengte] = size(c)

xaslengte = flengte + clengte   %lengte van de x-as dynamisch berekenen

%tekenen van de verschillende stemfuncties
%elk met een x-as gelijk aan xaslengte
hold on
subplot(3,1,1)
stem(f)        %discreet plot de stapfunctie 
axis([0 xaslengte min(f)-0.2 max(f)+0.2])%x-as van 0 tot de xaslengte, y-as van min-marge tot max+marge
subplot(3,1,2)
stem(c)        %discreet plot de coefficientfunctie
axis([0 xaslengte min(c)-0.2 max(c)+0.2])%x-as van 0 tot de xaslengte, y-as van min-marge tot max+marge
subplot(3,1,3)
stem(conv(f,c)) %discreet plot van de convolutie
axis([ 0 xaslengte min(conv(f,c)) max(conv(f,c))+0.2])
hold off