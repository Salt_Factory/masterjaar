%Author: Simon Vandevelde

%gegevens
fs=1000;            %Sampling rate van 1000 Hertz
Ts = 1/fs;          %De sampletime
n1 = 64;            %Het aantal samples
n2 = 512;
noise1 = randn(1,n1);      %random noise 
t1 = 0:1/fs:(n1-1)*Ts;   %64 samples aan 1000 Hertz is 0.064 seconden
                    %startwaarde:stap:eindwaarde
noise2 = randn(1,n2);      %random noise 
t2 = 0:1/fs:(n2-1)*Ts;   %512 samples aan 1000 Hertz is 0.512 seconden
                    %startwaarde:stap:eindwaarde
                    
                    
%golfvorm:
x1=100*sin(2*pi*100*t1) + 2*sin(2*pi*164.25*t1) + 1*noise1; %64 samples
x2=100*sin(2*pi*100*t2) + 2*sin(2*pi*164.25*t2) + 1*noise2; %512 samples

padding = zeros(1,512-n1)

y1 = [x1.*hamming(64).',zeros(1,512-64)]
y2 = x2.*hamming(512).'


t1 = 0:1/fs:(n2-1)*Ts;
subplot(2,2,1)
stem(t1,y1)
title('Discreet tijdsdomein: hamming met interpolatie')
xlabel('Time(s)')
ylabel('Amplitude')


subplot(2,2,2)
[Pxx,F] = periodogram(y1,[],n1,fs) %https://nl.mathworks.com/matlabcentral/answers/114942-how-to-calculate-and-plot-power-spectral-density-of-a-given-signal
plot(F,10*log10(Pxx))
title('periodogram met interpolatie')


subplot(2,2,3)
stem(t2,y2)
title('Discreet tijdsdomein: hamming zonder interpolatie')
xlabel('Time(s)')
ylabel('Amplitude')

subplot(2,2,4)
[Pxx,F] = periodogram(y2,[],n2,fs) %https://nl.mathworks.com/matlabcentral/answers/114942-how-to-calculate-and-plot-power-spectral-density-of-a-given-signal
plot(F,10*log10(Pxx))
title('periodogram zonder interpolatie')

%%code om discreet tijdsdomein te plotten:
%subplot(2,1,1)
%stem(y)
%title('Discreet tijdsdomein')
%xlabel('Time(s)')
%ylabel('Amplitude')

