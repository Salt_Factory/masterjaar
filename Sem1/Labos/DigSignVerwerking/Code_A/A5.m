%Author: Simon Vandevelde

%gegevens
fs=1000;            %Sampling rate van 1000 Hertz
Ts = 1/fs;          %De sampletime
n = 64;            %Het aantal samples
noise = randn(1,n);      %random noise 
t = 0:1/fs:(n-1)*Ts;   %64 samples aan 1000 Hertz is 0.064 seconden
                    %startwaarde:stap:eindwaarde

%golfvorm:
x=100*sin(2*pi*100*t) + 2*sin(2*pi*164.25*t) + 1*noise;

%padding
padding = zeros(1,512-n)

%signaal na het boxcarvenster
y = x.*boxcar(64).'

%%code om discreet tijdsdomein te plotten:
subplot(2,1,1)
stem(y)
title('Discreet tijdsdomein')
xlabel('Time(s)')
ylabel('Amplitude')


subplot(2,1,2)
[Pxx,F] = periodogram(y,[],n,fs) %https://nl.mathworks.com/matlabcentral/answers/114942-how-to-calculate-and-plot-power-spectral-density-of-a-given-signal
plot(F,10*log10(Pxx))
title('periodogram zonder padding')

%x = [x, padding]
%y = x.*[boxcar(64).',zeros(1,512-n)]


%subplot(2,1,2)
%[Pxx,F] = periodogram(y,[],n,fs) %https://nl.mathworks.com/matlabcentral/answers/114942-how-to-calculate-and-plot-power-spectral-density-of-a-given-signal
%plot(F,10*log10(Pxx))
%title('PSD met padding')