%Author: Simon Vandevelde

%Gegeven waarden
a = 10
b = 5
n = 32          %hoeveelheid samples
p = 500
q = 750
r = 5000

%Berekende waarden
fs = r          %samplefrequentie
Ts = 1/fs       %sampletijd

T = (n-1)*Ts    %laatste samplewaarde, x(t) bestuderen tot T
t = 0:1/fs:T;   %array maken van de verschillende tijden waarop gesampled wordt
                %startwaarde:stap:eindwaarde

x = a*sin(2*pi*p*t) + b*sin(2*pi*q*t)   %tijden invullen in de functie


%todo
subplot(4,1,1)
plot(t,round(x))
subplot(4,1,2)
plot(t,ceil(x))
subplot(4,1,3)
plot(t,fix(x))
subplot(4,1,4)
plot(t,floor(x))


