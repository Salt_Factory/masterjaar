%Author: Simon Vandevelde

%Gegeven waarden
a = 10
b = 5
n = 32          %hoeveelheid samples
m = 8          %aantal bits
p = 500

q = 750
r = 5000

F = [0,1]

%Berekende waarden
fs = r          %samplefrequentie
Ts = 1/fs       %sampletijd

T = (n-1)*Ts    %laatste samplewaarde, x(t) bestuderen tot T
t = 0:1/fs:T;   %array maken van de verschillende tijden waarop gesampled wordt
                %startwaarde:stap:eindwaarde

x = a*sin(2*pi*p*t) + b*sin(2*pi*q*t)   %tijden invullen in de functie

subplot(4,1,1)
hold on

m = 2
stapgrootte2 = 2*max(x)/2^m
quantiseerdSignaal2 = round(x/stapgrootte2)*stapgrootte2
quantisatiefout2 = quantiseerdSignaal2 - x
%round ruis
plot(t,quantisatiefout2)       %plotten van discrete sequentie

rms2 = rms(quantisatiefout2)

m = 4
stapgrootte4 = 2*max(x)/2^m
quantiseerdSignaal4 = round(x/stapgrootte4)*stapgrootte4
quantisatiefout4 = quantiseerdSignaal4 - x
%round ruis
plot(t,quantisatiefout4)       %plotten van discrete sequentie

rms4 = rms(quantisatiefout4)


m = 8
stapgrootte8 = 2*max(x)/2^m
quantiseerdSignaal8 = round(x/stapgrootte8)*stapgrootte8 
quantisatiefout8 = quantiseerdSignaal8 - x
%round ruis
plot(t,quantisatiefout8)       %plotten van discrete sequentie

rms8 = rms(quantisatiefout8)

m = 16
stapgrootte16 = 2*max(x)/2^m
quantiseerdSignaal16 = round(x/stapgrootte16)*stapgrootte16 
quantisatiefout16 = quantiseerdSignaal16 - x
plot(t,quantisatiefout16)       %plotten van discrete sequentie
rms16 = rms(quantisatiefout16)

legende2 = sprintf('2bit, rms = %f',rms2)
legende4 = sprintf('4bit, rms = %f',rms4)
legende8 = sprintf('8bit, rms = %f',rms8)
legende16 = sprintf('16bit, rms = %f',rms16)


legend({legende2, legende4, legende8, legende16})
title('round')
hold off



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subplot(4,1,2)
hold on

m = 2
stapgrootte2 = 2*max(x)/2^m
quantiseerdSignaal2 = fix(x/stapgrootte2)*stapgrootte2
quantisatiefout2 = quantiseerdSignaal2 - x
%round ruis
plot(t,quantisatiefout2)       %plotten van discrete sequentie

rms2 = rms(quantisatiefout2)

m = 4
stapgrootte4 = 2*max(x)/2^m
quantiseerdSignaal4 = fix(x/stapgrootte4)*stapgrootte4
quantisatiefout4 = quantiseerdSignaal4 - x
%round ruis
plot(t,quantisatiefout4)       %plotten van discrete sequentie

rms4 = rms(quantisatiefout4)


m = 8
stapgrootte8 = 2*max(x)/2^m
quantiseerdSignaal8 = fix(x/stapgrootte8)*stapgrootte8 
quantisatiefout8 = quantiseerdSignaal8 - x
%round ruis
plot(t,quantisatiefout8)       %plotten van discrete sequentie

rms8 = rms(quantisatiefout8)

m = 16
stapgrootte16 = 2*max(x)/2^m
quantiseerdSignaal16 = fix(x/stapgrootte16)*stapgrootte16 
quantisatiefout16 = quantiseerdSignaal16 - x
plot(t,quantisatiefout16)       %plotten van discrete sequentie

legende2 = sprintf('2bit, rms = %f',rms2)
legende4 = sprintf('4bit, rms = %f',rms4)
legende8 = sprintf('8bit, rms = %f',rms8)
legende16 = sprintf('16bit, rms = %f',rms16)

rms16 = rms(quantisatiefout16)
legend({legende2, legende4, legende8, legende16})
title('fix')
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subplot(4,1,3)
hold on

m = 2
stapgrootte2 = 2*max(x)/2^m
quantiseerdSignaal2 = ceil(x/stapgrootte2)*stapgrootte2
quantisatiefout2 = quantiseerdSignaal2 - x
%round ruis
plot(t,quantisatiefout2)       %plotten van discrete sequentie

rms2 = rms(quantisatiefout2)

m = 4
stapgrootte4 = 2*max(x)/2^m
quantiseerdSignaal4 = ceil(x/stapgrootte4)*stapgrootte4
quantisatiefout4 = quantiseerdSignaal4 - x
%round ruis
plot(t,quantisatiefout4)       %plotten van discrete sequentie

rms4 = rms(quantisatiefout4)


m = 8
stapgrootte8 = 2*max(x)/2^m
quantiseerdSignaal8 = ceil(x/stapgrootte8)*stapgrootte8 
quantisatiefout8 = quantiseerdSignaal8 - x
%round ruis
plot(t,quantisatiefout8)       %plotten van discrete sequentie

rms8 = rms(quantisatiefout8)

m = 16
stapgrootte16 = 2*max(x)/2^m
quantiseerdSignaal16 = ceil(x/stapgrootte16)*stapgrootte16 
quantisatiefout16 = quantiseerdSignaal16 - x
plot(t,quantisatiefout16)       %plotten van discrete sequentie

legende2 = sprintf('2bit, rms = %f',rms2)
legende4 = sprintf('4bit, rms = %f',rms4)
legende8 = sprintf('8bit, rms = %f',rms8)
legende16 = sprintf('16bit, rms = %f',rms16)

rms16 = rms(quantisatiefout16)
legend({legende2, legende4, legende8, legende16})
title('ceil')
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subplot(4,1,4)
hold on

m = 2
stapgrootte2 = 2*max(x)/2^m
quantiseerdSignaal2 = floor(x/stapgrootte2)*stapgrootte2
quantisatiefout2 = quantiseerdSignaal2 - x
%round ruis
plot(t,quantisatiefout2)       %plotten van discrete sequentie

rms2 = rms(quantisatiefout2)

m = 4
stapgrootte4 = 2*max(x)/2^m
quantiseerdSignaal4 = floor(x/stapgrootte4)*stapgrootte4
quantisatiefout4 = quantiseerdSignaal4 - x
%round ruis
plot(t,quantisatiefout4)       %plotten van discrete sequentie

rms4 = rms(quantisatiefout4)


m = 8
stapgrootte8 = 2*max(x)/2^m
quantiseerdSignaal8 = floor(x/stapgrootte8)*stapgrootte8 
quantisatiefout8 = quantiseerdSignaal8 - x
%round ruis
plot(t,quantisatiefout8)       %plotten van discrete sequentie

rms8 = rms(quantisatiefout8)

m = 16
stapgrootte16 = 2*max(x)/2^m
quantiseerdSignaal16 = floor(x/stapgrootte16)*stapgrootte16 
quantisatiefout16 = quantiseerdSignaal16 - x
plot(t,quantisatiefout16)       %plotten van discrete sequentie

legende2 = sprintf('2bit, rms = %f',rms2)
legende4 = sprintf('4bit, rms = %f',rms4)
legende8 = sprintf('8bit, rms = %f',rms8)
legende16 = sprintf('16bit, rms = %f',rms16)

rms16 = rms(quantisatiefout16)
legend({legende2, legende4, legende8, legende16})
title('floor')
hold off

figure(2)
plot([2 4 8 16], [rms2 rms4 rms8 rms16])



