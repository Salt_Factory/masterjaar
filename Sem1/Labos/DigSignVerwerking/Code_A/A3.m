%Author: Simon Vandevelde
%matlab file voor het onderzoeken van de invloed van sampling op het
%frequentiespectrum van een golvorm in het continue tijdsdomein

%Gegeven waarden:
a = 5
r = 8000
p = 1000

n = 64

%Berekende waarden:
fs = r                  %samplefrequentie
Ts = 1/fs               %sampleperiode
t = 0:1/fs:(Ts*(n-1))  %dynamisch de 64 stappen berekenen
freq = (0:(n-1))*fs/n
freqshift = freq - fs/2


%golfvorm:
x = a*cos(2*pi*p*t)


%afbeelden van golfvorm in discreet domein:
stem(t,x)

%FFT op gesamplede dataset
%stem(freq,abs(fft(x)))

%FFTshift
%stem(freqshift,fftshift(abs(fft(x))))

%plot(fftshift(x))