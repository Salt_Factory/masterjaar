%Author: Simon Vandevelde

%gegevens
fs=1000;            %Sampling rate van 1000 Hertz
Ts = 1/fs;          %De sampletime
ns = 64;            %Het aantal samples
noise = randn;      %random noise 
t = 0:1/fs:ns*Ts;   %64 samples aan 1000 Hertz is 0.064 seconden

%golfvorm:
x=100*sin(2*pi*100*t) + 2*sin(2*pi*164.25*t) + 1*noise;


%code om discreet te plotten:
plot(t,x)
title('Discreet tijdsdomein');
xlabel('Time(s)');
ylabel('Amplitude');