%Author: Simon Vandevelde

%Gegeven waarden:
a = 10
b = 5
n = 32          %hoeveelheid samples
p = 500
q = 750
r = 5000

%Berekende waarden:
fs = r          %samplefrequentie
Ts = 1/fs       %sampletijd

T = (n-1)*Ts    %laatste samplewaarde, x(t) bestuderen tot T
t = 0:1/fs:T;   %array maken van de verschillende tijden waarop gesampled wordt
                %startwaarde:stap:eindwaarde

%Golfvorm:
x = a*sin(2*pi*p*t) + b*sin(2*pi*q*t)   %tijden invullen in de functie


hold on         %de komende grafieken samen tekenen

stem(t,x)       %plotten van discrete sequentie

t = 0:1/(10*fs):T;  %tien keer meer samples
x = a*sin(2*pi*p*t) + b*sin(2*pi*q*t)   %functie herrekenen
plot(t,x)       %plotten van discrete sequentie alsof het continu was

hold off        %beide plots tekenen
