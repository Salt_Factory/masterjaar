%Author: Simon Vandevelde
%matlab file om 2e orde feedback differentievgl te analyseren

%gegeven waarden:
a1 = -1.767;
a2 = 1.04;

%y(n) = x(n) - a1*y(n-1) - a2*y(n-2)
%H(z) = 1/(1 + a1*z^-1 + a2*z^-2)

noemer = [1 a1 a2];
teller = 1;

%frequentieweergave weergeven, met 1000 punten
freqz(teller,noemer, 1000)

%zplane(teller,noemer)

roots(noemer)
%% %r laten evolueren naar 1

r = [0.9 0.99 0.999]%r = abs(z) -> abs(z) = 0.99
theta = 0.5229 %30 graden in radialen
for rz = r
    z1 = rz*exp(j*theta)
    z2 = conj(z1)
    teller = 1;
    noemer = poly([z1,z2])
    %zplane(teller,noemer);
    freqz(teller,noemer);
    titel = sprintf('r is %f',rz);
    title(titel);
    w = waitforbuttonpress;
end

%% %theta laten evolueren naar 1

r = 1.01978 
theta = [0 pi/2 pi]
for thetaz = theta
    z1 = r*exp(j*thetaz)
    z2 = conj(z1)
    teller = 1;
    noemer = poly([z1,z2])
    %zplane(teller,noemer);
    freqz(teller,noemer);
    titel = sprintf('theta is %f',thetaz);
    title(titel);
    w = waitforbuttonpress;
    
end

%% %stap-en impulsresponsie van het oorspronkelijke IIR netwerk


%gegeven waarden:
a1 = 0.5;
a2 = 0.25;


noemer = [1 a1 a2];
teller = 1;

stepz(teller,noemer,10);
title('Stapresponsie van IIR')
w = waitforbuttonpress;
impz(teller,noemer,10);
title('Impulsresponsie van IIR')



