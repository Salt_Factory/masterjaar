%Author: Simon Vandevelde
%matlab file om 2e orde feedforward differentievgl te analyseren

%gegeven waarden:
b1 = 0.5;
b2 = 0.25;

noemer = [1 0 0];
teller = [1 b1 b2];

roots(teller)
zplane(teller,noemer)

%% %r laten evolueren naar 1

%gegeven waarden:
b1 = 0.5;
b2 = 0.25;

noemer = [1 0 0];
teller = [1 b1 b2];

r = [0.9 0.99 0.999]%r = abs(z) -> abs(z) = 0.99
theta = 60*pi/180 %60 graden in radialen
for rz = r
    z1 = rz*exp(j*theta)
    z2 = conj(z1)
    teller = poly([z1,z2]);
    noemer = [1 0 0];
    %zplane(teller,noemer);
    freqz(teller,noemer);
    titel = sprintf('r is %f',rz);
    title(titel);
    w = waitforbuttonpress;
end

%% %theta laten evolueren naar 1

%gegeven waarden:
b1 = 0.5;
b2 = 0.25;

noemer = [1 0 0];
teller = [1 b1 b2];

r = 0.5
theta = [0 pi/2 pi]
for thetaz = theta
    z1 = r*exp(j*thetaz)
    z2 = conj(z1)
    teller = poly([z1,z2]);
    noemer = [1 0 0];
    %zplane(teller,noemer);
    freqz(teller,noemer);
    titel = sprintf('theta is %f',thetaz);
    title(titel);
    w = waitforbuttonpress;
    
end

%% %impulsresponsie van het oorspronkelijke IIR netwerk


%gegeven waarden:
b1 = 0.5;
b2 = 0.25;

noemer = [1 0 0];
teller = [1 b1 b2];

%stepz(teller,noemer,10);
impz(teller,noemer,10);
%dimpulse(teller,noemer);