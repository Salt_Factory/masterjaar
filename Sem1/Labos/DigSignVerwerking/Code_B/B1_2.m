%Author: Simon Vandevelde
%Matlab file die een filter modeleert plus ruis

%gegeven waarden:
n = 200;
p = 100;
r = 10000;
ruis = rand(1,n);
acoeff = [0.1, 0.5, 0.9, 1, 1.1];


%Berekende waarden:
fs = r
Ts = 1/fs

T = (n-1)*Ts    %laatste samplewaarde, x(t) bestuderen tot T
t = 0:1/fs:T   %array maken van de verschillende tijden waarop gesampled wordt
                %startwaarde:stap:eindwaarde


x = sin(2*pi*p*t) + ruis;

tellertje = 1
%berekenen filter:
for coeff = acoeff
    teller = 1; %noemer is 1
    noemer = [1,-coeff] %teller is 1-a1*z^-1
    y = filter(teller,noemer,x)
    
    subplot(5,1,tellertje)
    plot(t,y)
    
    hold on
    titel = sprintf('a = %d',coeff)
    title(titel)
    plot(t,x)
    legend('y(n): met filter','x(n): zonder filter')
    hold off
    tellertje = tellertje + 1;
end



