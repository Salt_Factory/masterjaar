%Author: Simon Vandevelde
%Matlab file om een filter te maken 
%met 200 keer een 1 als dataset

%gegeven waarden:
x = ones(1,200)
a1 = 0.1
a2 = 0.5
a3 = 0.9
a4 = 1
a5 = 1.1

%transfertfunctie: x = 1/(1-a1*z^-1)

%berekenen filter a1:
teller = 1; 
noemer = [1,-a1]
y = filter(teller,noemer,x)
subplot(5,1,1)
plot(y)
titel = sprintf('coeff = %s',a1);
title(titel);

%berekenen filter a2:
teller = 1;
noemer = [1,-a2];
y = filter(teller,noemer,x)
subplot(5,1,2)
plot(y)
titel = sprintf('coeff = %s',a2);
title(titel);


%berekenen filter a3:
teller = 1;
noemer = [1,-a3] 
y = filter(teller,noemer,x)
subplot(5,1,3)
plot(y)
titel = sprintf('coeff = %s',a3);
title(titel);

%berekenen filter a4:
teller = 1;
noemer = [1,-a4]
y = filter(teller,noemer,x)
subplot(5,1,4)
plot(y)
titel = sprintf('coeff = 1',a4);
title(titel);

%berekenen filter a5:
teller = 1;
noemer = [1,-a5]
y = filter(teller,noemer,x)
subplot(5,1,5)
plot(y)
titel = sprintf('coeff = %s',a5);
title(titel);