%Author: Simon Vandevelde
%Matlab file dat het poolzeroplot en de frequentieanalyse van een filter
%met verschillende coeff a tekent

%gegeven waarden:
acoeff = [0.1 0.5 0.9 1 1.1];


%berekenen filter:
for coeff = acoeff
    teller = [1] %teller is 1
    noemer = [1,-coeff] %noemer is 1-a1*z^-1

    zplane(teller,noemer)
    titel = sprintf('a = %d',coeff)
    title(titel)

    w = waitforbuttonpress; %anders wordt enkel de laatste getekend
    freqz(teller,noemer)
    title(titel)
    w = waitforbuttonpress;
end

close all