//Gemaakt door Dries
//om te testen of een pagetable-element voor Read & Write voor allebei dezelfde offset gebruikt of niet
//spoilers: het gebruikt dezelfde offset, wat het superawkward maakt
//mercietjes Dries!
#include <stdio.h>
#include <stdlib.h>
#define FILENAME "test.txt"
int main()
{
    FILE* f = fopen(FILENAME, "w");
    for (char i = 'A'; i <= 'Z'; i++)
    {
        fputc(i, f);
    }
    fclose(f);

    puts("> Before");
    system("cat " FILENAME);

    f = fopen(FILENAME, "r+");
    for (int i = fgetc(f); i != EOF; i = fgetc(f))
    {
        if (i < 'O') continue; // Skip the first half
        i ^= 0x20;
        fputc(i, f);
    }
    fclose(f);

    puts("\n> After");
    system("cat " FILENAME);
}
