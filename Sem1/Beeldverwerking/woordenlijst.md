# Woordenlijst Beeldverwerking

## Deel van Goedeme

### 01: Digital image fundamentals
|term|uitleg|
|---|---|
|low-level stage Computer Vision  | beeld in, beeld uit   |
|mid-level stage Computer Vision| beeld abstraheren naar features (bv lijnen, hoeken, punten)|
|high-level stage COmputer Vision| features analyseren |
|sampling | het digitalizeren van de coordinaatwaarden |
|quantisatie | het digitalizeren van de amplitudewaarden |
|gray-level reduction | Image quantization van de pixelwaarden|
|spatial reduction | Image quantization van de spatiale coordinaten |
|spatiale resolutie | aantal pixels in breedte en lengte |
|gray-level resolutie | aantal niveau's voor grijswaarden (standaar dbv 256) |

### Image Enhancement and FIltering
|term|uitleg|
|---|---|
|contrast stretching | Het verhogen van de dynamische range van grijswaarden | 
|gray-level slicing | Een specifieke range van grijswaarden highlighten |
|histogram equalization | techniek om de weergave van een slecht beeld te verbeteren, door het resultaathitogram zo vlak mogelijk te krijgen |
|logic operations | van toepassing op pixel-per-pixel (bv AND, OR) | 
|image subtraction | enhancement van de verschillen tussen images |
|image addition | enhancement van mergingen van afbeeldingen | 
|salt and pepper noise | willekeurige zwarte en witte pixels |
|impulsnoise | willekeurige witte pixels |
|gaussian noise| intensiteitsvariaties van een Gaussische normaalverdeling (meest voorkomend!)|
|kernel | hetzelfde als filter of masker|
|spatial filtering | filteren adhv het convolutiemechanisme | 
|smoothing linear filters | gebruikt voor blurring en ruisonderdrukking, vorm van smoothing spatial filters | 
|order-statistics filters | niet-lineaire filters die gebruik maken van het rangschikken van pixels (bv median filter), vorm van smoothing spatial filters |
|matched filter principe| filter in window lijkt op het gezochte patroon | 
|ringing-effect | het doorschot bij de afsnijrequentie van een laagdoorlaatfilter | 

### Chapter 3: Image Thresholding
|term|uitleg|
|---|---|
|bimodaal histogram | histogram waar duidelijk twee verschillende regio's op te onderscheiden is|
|Otsu's Method | Bij bimodaal histogram, zoek de threshold t dat de gewogen som van de in-groep variaties minimaliseert wanneer we de grijswaarden splitsen op waarde t (maakt een binair image)|
|Global Thesholding | Thresholden met een constante threshold voor de hele image |
|Local Thresholding| Thresholden met verschillende thesholds per locatie (Bv adaptive thresholding) | 
|adaptive thresholding  | algoritme dat threshold berekent voor kleine regio's van de image, goed wanneer er grote verschillen in belichting zijn|
|mean adaptive thresholding | adaptive thresholding dat gebruik maakt van de gemiddelde waarde van de buurpixels | 
|gaussian adaptive thresholding | adaptive thresholding dat gebruik maakt van de gewogen som (met gaussiaans window als gewichtjes) van de buurpixels | 
|morphologische operaties | kunnen enkel op binaire beelden!|
|dilatie| doet objecten "groeien" met een window, eenzame zwarte stippen zijn altijd weg, witte stippen worden groter|
|erosie | doet objecten "krimpen" met een window, eenzame witte stippen zijn altijd weg, zwarte stippen worden groter|
|opening | erosie gevolgd door dilatie, smooth contouren, breekt dunne verbindingen en dunne uitsteeksels | 
|closing | dilatie gevolgd door erosie, verbindt dunne breuken, vult lange dunne goten en vult gaten kleiner dan het element | 
|connected components | pixels die (al dan niet met tussenpixels) verbonden zijn en componenten vormen|
|4-connected components| pixels die verbonden zijn via de boven-, onder-, linker- of rechterbuur|
|8-connected components| 4-connected components + de diagonale (alle 8 buurpixels dus)|

### Chapter 4: Feature Detection
|term|uitleg|
|---|---|
|point detection| detecteren van geisoleerde punten (Matched filter principe!)|
|line detection | detecteren van lijnen (ook weer Matched Filter!)|
|edge detection | basis: zoeken naar een regio met grote tekenen van verandering, niet hetzelfde als lijndetectie| 
|AWGN | Additive White Gaussian Noise |
|zero-crossing property | bij de tweede-orde afgeleide zou een verbinding tussen de twee extrema de x-as in het midden van de edge snijden. Dit is handig om het midden van een edge te vinden (p4 voor uitleg)|
|Soorten edge detectoren | Sobel, Prewitt, Roberts, Laplaciaan, zero-crossings|
|LoG| Laplacian of Gaussian |
|Canny edge detector| Eerst smoothen met gaussiaanse convolutie, dan laplaciaanfilter toepassen, dan de zero-crossings zoeken|
|Hough-transformatie voor lijndetectie| Beeld transformeren naar a-b parameterruimte | 
|sparse matrix| matrix met maar een kleine hoeveelheid aan elementen die niet nul zijn (voordelig voor opslag & rekenwerk)|
|line detection and linking| Eerst piekdetectie met houghtransformatie, vervolgens kijken of er lijnsegmenten bij die pieken horen, en waar ze starten & eindigen | 

### Visietechnologie Deel 1: Belichting
|term|uitleg|
|---|---|
|Diffuse belichting | Minimale schaduw en reflecties, maar minder onderscheid in oppervlaktekenmerken. Bv: TL-buis|
|Diffuse axiale belichting | Schaduwvrij, gelijkmatig, weinig glans, maar moeilijke opstelling en lage helderheid. Bv: Axiale LED|
|Ringbelichting | Weinig schaudvorming, gelijkmatig, maar ringvormige glans en lastige opstelling. v: optische vezel of LEDring|
|Directionele belichting| sterke, gelijkmatige belichting maar schaduwvorming en glans|
|Directionele strijkbelichting | toont oppervlaktedefecten/-structuur, maar ook zware schaduwvorming en 'hot spots'|
|Gepolariseerd licht | gelijkmatig, geen glans maar een lagere helderheid door de filter|
|Gestructureerd licht | benadrukken van oppervlakte kenmerken, maar moeilijkere bron en soms kleurabsorptie|
|Gelijkmatige belichting | Bv een LEDring|

### Image Formation
|term|uitleg|
|---|---|
|diffractie| "verzacht" de afbeeldingen, meer diffractie met hogere F/#|
|chromatic aberration | kleuren vallen verkeerd in|
|vignetting| buitenste rand donkerder|
|barrel distortion | negatieve lensdistortion |
|pincushion distortion | positieve lensdistortion |
|CCD| Charged Couple Device (nadelen: inerlacing, blooming, smearing)|
|CMOS| Complementary Metal Oxide on Semiconductor |
|Debayering| HEt interpoleren van een Bayerfilter, ook wel demosaicing genaamd|

### Chapter 4: Color Image Processing
|term|uitleg|
|---|---|
|radiance| de totale hoeveelheid energie dat van de lichtbron vloeit (in Watt)|
|luminance| Een meting van de hoeveelheid energy een observator ziet van een lichtbron (in Lumen)|
|brightness| subjectieve beschrijving dat quasi onmogelijk te meten valt, komt overeen met intensiteit van kleur|
|kleurmodel| een kleurmodel in essentie is een specificatie van een coordinaatsysteem en een subruimte in dat systeem waar elke kleur voorgesteld wordt met een enkel punt|
|hue| attribuut geassocieerd met de dominante golflengte in een mix van licht. Hue beschrijft een pure kleur (rood, geel, oranje, ...)|
|saturation| de relatieve puurheid of de hoeveelheid wit licht gemixt met een hue. Pure spectrumkleuren zijn volledig saturated.
|CIE| Commision Internationale de l'Eclairage|
|RGB| Red Green Blue|
|CMY| Cyan Magenta Yellow|
|CMYK| Cyan Magenta Yellow Black|
|HSI| Hue Saturation Intensity|
|YUV| Y: helderheid van de kleur U&V: de eigenlijke kleur (de chroma) (als Y = 1: dan is het sws wit!)|
|Kleurenzeshoek voor HSI/HSV | Hue is een hoek tusen 0&2pi, S&I lopen tussen 0 en 1|
|YUV444| 4 bits Y, 4 U, 4 V |
|Eerste categorie van kleurenbeeldprocessing| Elk componentenbeeld individueel processen ahv standaard gray-scale processmethodes en we vormen vervolgen een samengesteld kleurenbeeld (per-color-component processing)|
|Tweede categorie van kleurenbeeldprocessing| Direct werken met de kleurenpixels (vector-based processing)|
|Color slicing | Een specifieke regio van kleuren in een beeld highlighten (thresholding in 3 dimensies, kan met bv kubus, bol, vage andere 3d-figuren)|
|dataset bias | algoritme werkt voornamelijk goed op eigen dataset|
|K-means clustering| manier om te clusteren |
|Jianbo Shi's Graph-Partitioning| Deelt een image op in gelijkaardige sets|
|Swain and Balled Histogram Matching| Histogram matching voor kleurenbeelden|

### Template Matching
|term|uitleg|
|---|---|
|template matching| techniek voor objectherkenning, vergelijkt delen van beelden met elkaar, op een pixel-by-pixel basis |
|OCR| Optical Character Recognition|
|Bi-Level Image TM| Template is een kleine bi-level image, template zoekt in source image met een yes/no aanpak|
|Grey-Level Image TM| Ipv yes/no wordt het verschil in niveau's gebruikt (Euclidische distance, correlatie, NCC, ..)|
|Euclidian Distance | Het verschil in afstand tussen de waarden|
|Correlation | Een meting van de gradatie waarin twee variabelen overeenkomen, niet in effectieve waarde maar in algemeen gedrag|
|Invloedsfactoren| Schaal, rotatie, translatie, belichting, occlusie, objectvariatie, speculariteiten |
|NCC| Normalized Cross Correlation, fancy vorm van correlatie|
|Parallax| Verschuiving tussen twee beelden van objecten|
|NCC StereoMatching| Stukjes uit beeld verschuiven op ander beeld tot template match | 

### Local Invariant Image Features
|term|uitleg|
|---|---|
|Invariant | kan er volledig tegen (NIET HETZELFDE ALS ROBUUST! Invariant is 100% er tegen kunnen)|
|Featurematching| twee stappen: keypoints abstraheren, gevolgd door keypoint matching|
|SIFT|scale-invariant feature transform|
|Harris Corner detector | Zoekt hoeken op basis van verandering in de edge-richting, translatie-invariant|
|SURF| Speeded up robust features |
|Integral Image| Een voorstelling voor een beeld waarbij de som van grijswaardepixels over een image gegeven wordt|
|HoG | Histogram oriented gradient|
|DoG | Difference of Gaussians|


### Structural Object Recognition
|term|uitleg|
|---|---|
|RANSAC| RAndom SAmple Concensus|
|true positives | # correct gedetecteerde matches |
|false positives | # fout gedetecteerde matches |
|ROC curve| Receiver Operating curve, zet de false positive rate uit tegenover de true positive rate|
|confusion matrix | matrix die "True" en "False" uitzet tegenover "Positive" en "Negative"|
|true positive rate |= Recall, = TP/(TP+FN)|
|false positive rate |= fall-out, = FP/(FP+TN)|
|ratio| soort van confidence meting, bovenop threshold dus extra streng, minder False Positives|
|SSD|Squared Summed Distance|

### Object classification concepts
|term|uitleg|
|---|---|
|Object classificatie| Herkennen van een bepaalde klasse van objecten |
|Objectherkenning | herkennen van een bepaalde instantie van een object |
|featurevector| een vector van metingen |
|p(x\y)| waarschijnlijkheid voor een objectklasse y, gegeven de featuremeting x |
|detection rate| kans dat een feature van een object geclassificeerd wordt als object |
|false positive rate | kans dat een feature van de achtergrond geclassificeerd wordt als object|

### Face Detection using the Viola-Jones Method
|term|uitleg|
|---|---|
|Viola-Jones | Door meerdere iteraties van classifier zorgen we ervoor dat de data die slecht gedetecteerd werd een grotere kans heeft om correct bevonden te worden door volgende classificatie|
|perceptron| zo een opgedeeld vierkantje |

### AAA Vision: Viola & Jones
|term|uitleg|
|---|---|

### Deep Learning for Computer Vision
|term|uitleg|
|---|---|
|supervised learning| end-to-end met backpropagation, op gelabelde data|
|unsupervised learning| leren met weinig specifieke informatie over het beeld (geen labeltjes enzo, en data waar niet mooi de shit uitgecropped is)|
|tensor| driedemensionale vector|
|ReLu| rectified linear|

### Supervised Deep Learning
|term|uitleg|
|---|---|
|generaliseerbaarheid van een netwerk | correct werken op dataset dat niet deel uitmaakte van de leerdata|
|Forward Propagation | Het proces van het uitrekenen van de output van een netwerk, gegeven zijn input |
|loss | hoe slecht is het netwerk bezig |
|Backward Propagation | Procedure om de gradienten van de loss tegenover de parameters in een multi-layer netwerk te berekenen|
|transfer learning | niet vanaf nul trainen, maar een pretrained net hertrainen|
|fully connected layer | elke input gebruikt iedere output van de vorige laag|
|pooling layer| het filteren van de responses op meerdere locaties|


### You Only Look Once
|term|uitleg|
|---|---|

## Deel van Vandewalle

### Geometric Image formation
|term|uitleg|
|---|---|
|inhomogene coordinaten | gewone coordinaatjes zoals we ze kennen en liefhebben |
|homogene coordinaten| coordinaten met een extra schaalfactor w bij | 
|#DoF|aantal vrijheidsgraden|
|camera intrinsics | configuratie van de camera, relatief tegenover de camera|
|camera extrinsics | positie v camera + orientatie (als camera ergens staat, hoe vertaal ik 3D in camera naar 3D in wereld)|
|over-water|geen andere interfererende objecten|
|Manhattan world | alles is recht op recht, verticaal, ...|


### Stereo vision and epipolar geometry
|term|uitleg|
|---|---|
|stereo| Griekse stereos wat "vast" betekent |
|stereo vision | twee views tesamen laat ons toe om de 3D positie van een scenepunt in te schatten |
|correspondence geometry | epipolaire gemoetrie, gegeven een punt x in de eerste afbeelding, hoe beperkt het punt x' in de tweede view |
|camera geometry (motion) | Gegeven een set van corresponderende punten xi, x'i wat zijn de cameramatrices van de twee views?|
|scene geometry | gegeven de overeenkomstige punten xi, x'i en de cameramatrices P, P', wat is de positie van X in 3d?|
|epipolaire lijn| een punt in een view definieert een epipolaire lijn in een andere view, waarop het corresponderende punt ligt |
|fundamental matrix F| de 3x3 matrix die de epipolaire geometry voorsteld|
|baseline| de lijn die door de epipolen van beide images gaat (e en e')|
|epipolair vlak | ieder vlak waar de baseline deel van is (allemaal gelijk op 1 parameter na)|
|epipole| het snijpunt van de lijn die de cameracenters en de imageplane samenvoegd|

### Disparity Estimation
|term|uitleg|
|---|---|
|structured light cameras| lichtbron vervangt 1 camera (vaak is de lichtbron IR) welke een patroon projecteerd, waarna de afstand berekend kan worden|
|Time-Of-Flight cameras| meten de tijd tussen uitgestuurd en ontvangen lichtpulsje |
|Lidar| LIght Detection And Ranging|

|techniek| passief of actief?| voordelen| nadelen|
|---|---|---|---|
|Stereocamera's| passief | geen speciale belichting nodig, twee 'standaard' camera's | moeilijk op homogene oppervlakken, kwaliteit hangt af van de baseline, occlusieregio's zijn moeilijk|
|structured light camera's | actief |niet afhankelijk van oppervlaktestructuur | gevoelig aan interferentie met omgevingslicht, occlusieregio's |
|time-of-flight camera's | actief | dieptebeeld is gealigneer met lichtbeeld, geen minimum baseline nodig | hangt af van interferentie met omgevingsbelichting, ambiguiteit door phase wrapping, dieptebeeld vaak lagere resolutie, meerpadige reflecties kan noise veroorzaken |
|Lidar | actief |zie ToF | zie ToF | 

### TV Processing
|term|uitleg|
|---|---|
|motion-compensated frame rate conversion | estimate de beweging van de hele videograme, interpolate de tussenframe gebruikmakende van bewegeingscompensatie|
|HDR| High dynamic range, hoger contrast dan vroeger|


|techniek voor 3D| voordelen | nadelen |
|---|---|---|
|Parallax| het werkt, kan uitgeschakeld worden om gewone 2D tv te zien| veel helderheidsverlies!|
|Lenticulair| Alle helderheid in uw bakkes | niet uit te zetten (tenzij met LC laag, wat kostig is! :() |
|Dir. Backlight | Geen idee!| Geen idee! (Als iemand kan aanvullen please do)|
Alle drie hebben een juiste positie van de kijker nodig! In werkelijkheid maken ze vaak meer aanzichten, maar das dik moeilijk en dik tijdrovend.

Processing die aanwezig is in een tv:
* upscaling
* FRC (frame-interpolatie)
* WCG proc (colour generation, om de range te kunnen bereiken)
* ambilight (mjoh)
* videodecoder
### 3D sensing
|term|uitleg|
|---|---|


## Misc brol

### Basisfilteroperaties
* Puntoperaties
    * Grayscale/colour slicing
    * Arithmetic
    * Inverteren
    * Histogramprocessing
* Spatiele filters (met masker)
    * smoothing filters
        * mean
        * gaussian
        * median
    * sharpening filters
        * gradient
        * Laplaciaan (gradient in alle richtingen)
* Frequentiedomein filtering
    * LPF, HPF, ...

### Image thesholding:
* Globale threshold
    * Otsu's methode bij bimodaal histogram
    * los op de gok lmao
* Adaptive thesholdin (lokaal)
    * mean adaptive thresholding
    * Gaussian adaptive thresholding

### Morphologische operatoren
* Dilatie (aandikken)
* Erosie (eroderen wow)
* Opening (erosie, gevolgd door dilatie)
* Closing (deilate, gevolgd door erosie)

### Methodes voor Connected Component Analysis
* Recursive tracking (niet veelgebruikt)
* parallel growing (parallellisme nodig)
* Row-by-row
    a. labels naar beneden propageren, en de equivalenties onthouden
    b. de equivalentieklassen berekenen
    c. elke label met zijn equivalente klasse veranderen

### Methodes voor edge-detection
* Roberts (geen centerpixel)
* Sobel 
* Prewit
* Laplaciaan (in de vorm van Laplacian of Gaussian (LoG, Mexican hat function))
* zero-crossing methodes (bv thesholden na het toepassen van LoG, Canny edge)

### Methodes voor line-detection
* Houghtransformatie
* Fancy Hough dat naar andere parameterruimte gaat

### Verschillende soorten belichting en voordelen, nadelen

|belichting | voordelen | nadelen| voorbeeld |
|---|---|---|---|
|Diffuse belichting | Minimale schaduw en reflecties | minder onderscheid in oppervlaktekenmerken | TL-buis|
|Diffuse axiale belichting | Schaduwvrij, gelijkmatig, weinig glans | moeilijke opstelling en lage helderheid | Axiale LED|
|Ringbelichting | Weinig schaduwvorming, gelijkmatig| ringvormige glans en lastige opstelling |optische vezel of LEDring|
|Directionele belichting| sterke, gelijkmatige belichting |schaduwvorming en glans| glasvezel geleid licht |
|Directionele strijkbelichting | toont oppervlaktedefecten/-structuur | zware schaduwvorming en 'hot spots'| glasvezel geleid licht |
|Gepolariseerd licht | gelijkmatig, geen glans |een lagere helderheid door de filter| lichtbron + filter |
|Gestructureerd licht | benadrukken van oppervlakte kenmerken |moeilijkere bron en soms kleurabsorptie| patroon-, lijnprojectie met laserdiodes of via optische vezel |
|Gelijkmatige belichting ||| Bv een LEDring|


### Methoden van Template Matching
* Bi-Level
    * Yes/No aanpak
* Gray-level
    * Euclidische afstand
    * Correlatie
    * Normalised Cross Correlation (NCC)

### Manieren om meer correcte matches te bekomen bij object recognition adhv keypoint (meer true positives & meer true negatives)
* Threshold op het ratio van de beste en de 2debeste match (zoals in het labo van Philips)
* Two-way matching (niet in labo Philips)
* Structural outlier rejection (Bv RANSAC, zoals enkel Dries, Dylan en Olivier in labo Philips lmao)


### Tabel met opsomming van robuustheden en shit
|techniek| Schaal | Rotatie | Translatie | Belichting | Occlusie| Objectvariatie | Speculariteiten|
|---|---|---|---|---|---|---|---|
|Kleurhistorgram| nee | ja | ja | nee | nee | nee | nee |
|Template Matching | nee | nee | ja | ja, mits normalisatie | nee (/deels) | nee | nee |
|Invariant Local Features (featurematching)| ja | ja | ja | ja? | ja (door lokaliteit) | nee (of toch niet super)| ja|
|---|---|---|---|---|---|---|---|
|detectoren|
|---|---|---|---|---|---|---|---|
|Harris Detector | nee | ja | ja | deels | ja | soort van | niet vermeld|
|Harris-Laplaciaan Detector | ja | ja | ja | deels | ja | soort van | niet vermeld|
|SIFT| ja | ja | ja | ja | niet vermeld | ja| niet vermeld | 
|SURF| ja | ja | ja | ja | niet vermeld | ja| niet vermeld | 
|---|---|---|---|---|---|---|---|



### Manieren om aan objectherkening te doen:
* Template Matching
* Kleurhistogram
* Local Image Features

### Voorbeelden van classifiers (delen een featurevectorruimte op in regio's)
* Nearest class mean
* Nearest neighbor
* Bayesian classifier
* Neural Network
* Support Vector Machine (SVM)

### Duuzend handige link waar Convnets nog eens worden uitgelegd zonder het woord "stoer" om de 5 minuten te moeten horen
https://ujjwalkarn.me/2016/08/11/intuitive-explanation-convnets/

### Hierarchie van 2D transformaties
|transformatie | #vrijheidsgraden | verandert | behoudt |
|translatie | 2 | x,y | al de rest (orientatie)|
|Euclidisch| 3 | x,y, rotatie tau| de afstanden enzo |
|similarity | 4 | x,y, rotatie tau, schaalfactor s | de hoeken |
|affine | 6 | x,y,rotatie tau, schaalfactor s, twee andere waarden | parallellisme|
|projective | 8 | alles | rechte lijnen|
