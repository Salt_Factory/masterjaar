p5:
-----


Ondernemingsrecht is vooral ondernemingen onderling, waar in economisch recht ok tussenomst tussen overheid in ondernemingsebeuren (intervenerend & corrigerend, voorkomt al te grote marktvergroting, zorgt voor regularisatie, ...)

Wetboek v vennootschappen wordt vereendvoudigd na 1 november

p6:
-----
B.: Handelaar vroeger: iemand die doet aan daden van koophandel
Handelaar later: standaard onderneming, welk op meer betrekking heeft dan handelaar. Notaris, dokter, .. zijn nu ondernemingen (want economische activiteit) 

Objectieve daden: dingen die iemand handelaar maken
-> stonden in het wetboek

p7:
-
onrechtmare daad: bijvoorbeeld ongeval, je moet de schade vergoeden -> doordat subjectief moet 'de zaak' betalen voor de schade

p8:
-
Landbouwer kan nu bijvoorbeeld ook failliet gaan

p9:
-
BVBA vs Naamloze Vennootschap
BVBA eerser personenvennootschap, gesloten structuur
Naamloze is eerder anoniemer, open structuur

p10:
-
Vereniging heeft geen rechtspersoonlijkheid, kan niet als entiteit aangesproken worden juridisch

In Belgie: de vakbonden hebben geen rechtspersoonlijkheid, schadevergoeding eisen is zo goed als onmogelijk.


Authentieke akte is akte gemaakt door tussenpartij bv notaris -> heeft ruimere waarde

Onderhands is bijvoorbeeld koop van 2dehandswagen, tussen twee partijen opgesteld, zonder tussenkomst

p11:
-
Probleem met lekenrechters: 
stel verkoop van onderneming, waarbij verkocht onderneming bodemsaneringprobleem heeft.
->staat volledig recht van het commerciele, de lekenrechters moeten zich buigen over iets waar ze geen kennis van hebben

p12:
-
telkens twee kansen bij voorleggen van zaak aan rechter: in beroep gaan naar een hogere rechtbank indien men niet akkoord gaat


p16:
-
Particulieren moeten andere partij formeel in gebreke stellen, maar tussen ondernemingen onderling en met de overheid gaan automatisch de intresten lopen. De ondernemingen hoeven niemand in gebreke te stellen.

Extra contractuele afspraken kunnen wel onredelijk gesteld worden door een rechter (bv een rentevoet van 20%), welke dan door de rechter verlaagd kunnen worden naar de defaultwaarden.

Advokaat van winnende partij moet niet betaald worden door verliezende partij, maar wel een forfaitaire vergoeding.

Vroeger was deze vergoeding 'symbolisch', tegenwoordig zijn het vrij gesubstantieerde bedragen. -> middel om partijen te ontraden om willy-nilly te procederen!


p17:
-
Mag iedereen zomaar ondernemen? 
->Vrijheid van handel
->Juridische bevoegdheid
->Handelingsbekwaam 
Handelingsbekwaam is iedereen vanaf de leeftijd van 18 jaar (soms op vroegere leeftijd voor sommige contracten). Het heeft te maken om rechten te kunnen uitoefenen.
Rechtsbekwaamheid: vanaf de geboorte heeft een mens rechten (en eventueel ook plichten). Zoals bijvoorbeeld erfgenaam zijn. ->eventueel ook vanaf levensvatbaarheid

p18:
-
Vroeger moest men naar rechthandel van koophandel om op secretariaat alles aan te geven, tegenwoordig naar erkend ondernemersloket.

p19:
-
Tegenpartij moet u altijd om correcte manier kunnen identificeren

Verplichting van rekeningnummer is om zwartwerk tegen te gaan

p20:
-
Geen onderscheid tss prive en ondernemingsvermogen voor een eenmanszaak, zorgt voor extra risico! 
->huwelijk waarin alle vermogen gedeeld wordt, loopt risico om alles te verliezen. :(
->daarom bij eenmanszaak best 'scheiding van goederen' voor het huwelijkcontract, zodat enkel de uitbater's vermogen aanspreekbaar is.

Elke ondernemer vandaag is verplicht om sociale zekerheid te regelen


p22:
-
Klasse 3: mag gewoon, risicovrij, moeten niet expliciet toegelaten worden maar wel geafficheerd
Klasse 1&2: Expliciete toelating nodig

p25:
-
Vennootschap kan ook eenmanszaak zijn (wat op zich een tegenspraak is) 
->zodat beroepen als dokter, artsen, ... ook een vennootschap konden starten!

p26:
-
Wetboek van vennootschapsrecht is dringend aan herziening toe!

Maatschap kan niet naar de rechtbank genomen worden, enkel de leden van de maatschap


p27:
-
Bvba: ->personenvennootschap
* zaakvoerder vereist
* kan niet zomaar overgebracht wrden
* Niet zomaar iedereen kan kapitaal toevoegen, enkel de bestaande aandeelhouders
* Meer belasting 'als natuurlijk persoon', hoe meer verdient hoe meer belast


NV: ->kapitaalvennootschap 
* Vrij anoniem karakter
* algemene vergadering: iedereen die geld in de NV heeft gestopt
* dagelijks bestuur: bij grote ondernemingen worden afgevaardigden gestuurd!
* Iedereen kan financieren
* forfaitaire tarieven bij belasting: belasting blijft dus gelijk onafhankelijk van inkomen!

vennootschappen na de hervorming:
* maatschap
* (e)bvba  -> bv
    * wordt de personenvennootschap bij uitstek
* nv 
* cooperatieve vennootschap



p29:
-

eerste criteria: personenvennootschap vs kapitaalvennootschap


Kiezen voor een vennootschap is het creeren van een soort vehikel dat als aparte juridische entiteit fungeert
't is juridisch te beschouwen als aparte identiteit
Maw:
* kan er een contract mee sluiter
* vennootschappen moeten natuurlijke personen aanduiden die de contracten sluiten
* er is sprake van het 'vermogen van de vennootschap', niet het 'vermogen van de aandeelhouders'
* vennootschap 'woont' op eigen domicilie

<-> maatschap heeft geen aparte juridische entiteit:
    -> om een maatschap voor de rechtbank te slepen moet iedere persoon apart verschijnen

p30:
-

een vennootschap is eigenlijk een contract
-> overeenkomts tss verchillende personen bereidt om samen te ondernemen

Stel bij overleiden van tweede persoon: pluraliteitsvereiste onvoldaan!

Dwaling: een overeenkomst gesloten, maar daarbij niet de juiste informatie gekregen, met de juiste informatie zouden we geweigerd hebben.

oorzaak: zo vaag en ruim mogelijk zodat hier niet over gekut kan worden!

goederen in nature moeten wel geschat worden

Wie in een inbreng doet, krijgt in ruil daarvoor aandelen van vennootschap.
Aandelen van vennootschap drukken de waarde van de vennootschap uit.
Deze kunnen in waarde stijgen!

p31:
-

leeuwenbeding: winst aan bepaalde aanheelhouders uitdelen, ten laste van de anderen

winstbewijs: aparte betaling voor mensen die iets betekent door speciale diensten hebben voor de vennootschap


p32:
-

inbreng van goed of grond moet in 1 schijf gebeuren: natura moet volledig van de eerste keer ingebracht worden

p33:
-

BVBA kan geen dividenten uitbetalen ->geen preferente aandleen mogelijken

Alle aandelen staan ook op naam!
Men kan perfect nagaan wie allemaal aandeelhouders zijn!

Belangrijk! De overdracht van aandelen:
* Aandeelhouders mogen niet zomaar verkopen
* Stel: 5 vennoten, dus minstens 3 nodig
* voor verkoop van 10%, is totaal van 85% verdeeld nodig 


p34:
-

preferent: mate waaraan winsten aan aandelen gekoppeld worden:

p35:
-


* liquidatiebonus: bonus bij liquidatie van de vennootschap

* gedematerialiseerd: aandelen zijn niet meer fysiek, maar staan op rekeneningen

* Obligatiehouders worden altijd als eerste betaald!

* Vennoot en aandeelhouder zijn synoniemen!

p36:
-

Buitenlandse bedrijven werken vaak met een zusterkantoor.


p37:
-


p38:
-

Raad van bestuur is belangrijkste orgaan van NV
* Collegiaal: alles wordt beslist als 1 orgaan!
* Altijd met minstens 3
* Geen specifieke benoembaarheidsvereisten: geen beperkingen op basis van niet hebben van diploma, ...
* Ook rechtspersonen (bv een ander vennootschap) kunnen zetelen in een bestuur, met een vertegenwoordiger

* nieuwe bestuurders aanstellen gebeurd altijd door de raad van bestuur

bestuur: interne werking van een vennootschap -> INTERN
vertegenwoordiger: externe werking, naar buiten toe -> EXTERN

p41:
-

Belangenconflict:
* moet gemeld worden aan de andere bestuurders!
* Oftewel zich afzijdig houden tijdens beslissing, oftewel kunnen verantwoorden waarom het geen belangenconflict is

Fout op het interne vlak = van contractuele aard
Fout op het externe vlak = buiten contractuele aard

p42:
-

Raad van bestuur wordt zelf aangesteld door eigenaars
-> dit zorgt ervoor dat actio mandaat weinig voorkomt (waarom zouden ze hun eigen bois wegwillen)

Het welslagen van de onderneming is het risico van de onderneming zelf! Dit kan niet gebruikt worden tegen het bestuur!

#p62 p2.3 valt weg! Hoezee! 
Voor de rest is alles te kennen.
Zeker Toledo eens checken want hier zouden overzichten + woordenlijst + andere moeten komen!
