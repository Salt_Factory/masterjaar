# dinsdag 27 november

## p101
* Concurrentiestrategie = positieverbeterende strategie
 
* ondernemingsstrategie: er wordt beslist in welke martken er zal worden geconcurreerd
* concurrentiestrategie: welk concurrentieel voordeel streven we na? -> SCA
* functionele strategie: Het beleid t.a.v e-commerce, marketing, personeel, ..
 
* Bij KMO's vallen ondernemingsstrategie en concurrentiestrategie vaak samen

* Strategische inhoud: bv 'We gaan ons onderscheiden door service'
* Strategisch proces: bv na een omgevingsanalyse, de sterke en zwakke punten van de concurrenten 

### Ex: Wat is het verschil tss strategisch proces & strategische inhoud

* SWOT analyse: Strenghts, Weaknesses, Opportunities & Threats = de Externe analyse & Interne analyse 

* Bedrijfsstrategie: op welke markten wordt ik actief?
* Markt is: WAT WIE WAAR: Wat ga ik aan Wie en Waar aanbieden?
    * Onze opleiding: WAT = masters, WIE = ASO of TSO studentjes, WAAR = rond Sint-Katelijne-Waver
    * Nokia vroeger: rubberen laarzen, wc-papier
    * Siemens: Stopt met witgoed (Bussiness 2 consumermarkten) -> teveel Chinese namaak, past bedrijfsstrategie aan naar markt voor business2business schaliegaas & compressoren

* timingprobleem: de markt is nog niet klaar (bv: zelfrijdende wagen) -> TE VROEG
* timingprobleem: de markt is al bezet (bv: Blokker & e-commercei, de Kodak) -> TE LAAT

### Ex: Wat wordt bedoelt met de timing van de ondernemingsstrategie


* 5 p's van strategie niet verwarren met 4 p's van marketing

* Strategie als plan: een bewust gevolgde koers om de langetermijndoelen te realiseren
* Strategie als patroon: rode draad doorheen de organisatie
* Strategie als positie: Plaats naar waar men streeft binnen de concurrentiele omgeving 
* Strategie als perspectief: een visie op bedrijf, industrie en wereld
* Strategie als ploy:

* Mintzberg zijn ding niet kennen

### Oefening: bedrijf heeft pampers, treinen & bankactiviteiten
* moet het alles nog bijhouden?
* per activiteit twee criteria in kaart brengen:
    * relatief marktaandeel: het eigen marktaandeel gedeeld door marktaandeel grootste concurrent
        * als het rel marktaandeel > 1: dan hebben we een hoog rel. markaandeel, anders een klein.  -> drempelwaarde kan natuurlijk verschillen 
    * marktgroei: toekomstige jaarlijkse groei volgende 6 jaar:  hoeveel % gaat de markt groeien in de volgende 5 jaar
        * drempelwaarde: 10% -> ook aanpasbaar op zich 

Hieruit volgen 4 combinaties:
* Melkkoe (of goudmijn)
    * levert veel winst op
    * vereist weinig investering
* Ster
    * Levert veel winst op
    * minder gunstig dan melkkoe
    * veel geld nodig om de winsten te behouden
* Probleemkind
    * te lagen (weinig) winst
    * slorpt veel cash op
* Hond
    * Geen winst of verlieslatend
    * slorpen veel cash op

## Oefening slides
* Softwareafdeling: probleemkind
* Hardware: ster
Advies: hardware is beter dan software
    * we kunnen op de softwaremarkt een overname doen (DUUR) 
    * we kunnen de softwareafdeling doorverkopen

* Oefening 1 & 2: zelf maken en afgeven indien gewensd

* Desinvestering = afstoten van bedrijfsonderdelen

### Slide met al die vage ballen valt keihard weg!

## Groeistrategie van Ansoff:
* Ontzettend veel gebruikt in de praktijk

* Voorbeeld: reclames die aanzetten tot meer poetsen dan 2 keer per dag, gratis tandenborstels uitgeven, nieuwe toepassing voor tandpasta: witmaker

* Voorbeeld productexpansie:
    * nieuwe eigenschappen: GameBoy naar GameBoy Advance SP
    * nieuwe generatieproducten: Blu-Ray speler
    * productlijn verlengen: flosdraad

* Voorbeeld marktexpansie:
    * nieuwe distributiekanalen (manier om product aan klant te brengen): webshop voor luxemerken
    * nieuwe gebruikers aantrekken: manty (panty voor mannen)
    * Prijs verlagen: iphone voor 15 euro
    * Geografisch uitbreiden: BMW in China, Netflix

* Voorbeeld diversificatie:
    * Samsung maakt allerlei brol

* Diversificatie is de meest risicovolle strategie!
    * maar 1 op 5% van de pogingen slaagt!

## Concurrentiestrategieen
* SCA = Strategic Competitive Advantage
    * Moet een succesfactor zijn
        * Slechte SCA voor unief: alle proffen hebben gele sokken aan
    * Voordeel moet groot genoeg zijn
    * Moet duurzaam zijn (moet lang genoeg geld kunnen verdienen, niet gemakkelijk namaakbaar door concurrenten)

### Voorbeeld concurrentiestragieen
| Bedrijfsnaam 	|  Concurrentieel   voordeel 	|  Winstmarge = bedrijfswinst/omzet(%) 	|
|--------------	|----------------------------	|--------------------------------------	|
| 1. Colruyt   	| Laagste prijs              	| 5.2%                                 	|
| 2. Delhaize  	|  Kwaliteit Versheid        	| 4%                                   	|
| 3. Carrefour 	| ??? -> onbekend!           	| 3.1%                                 	|

* Des te meer voordeel tegemoet komt aan drie voorwaarden, des te beter

* toepassing v Zara valt weg!

* Endogene verstoringen: Door veranderingen ni de industrietak (technologi)
    * bijvoorbeeld floppy's: voordeel v goedkope opslag is weg, door komst USB
* Exogene verstoringen: Door wijzigingen in de voorkeur van de consument
    * bijvoorbeeld Lego: keiveel computerspellekes
    * interesse voor gezonde frisdranken: Coca Cola heeft Chaudfonaine overekocht

* NIET KENNEN: FOCUS OF SEGMENTATIE

* Kostenleiderschap: 
    1. Tegen zeer lage kostprijs produceren
    2. Verkopen tegen de marktprijs (Daardoor een hogere marge!)
    3. Kwaliteit nastreven die gelijk is aan de kwaliteit van de doorsneeproducent
    * Bv Colruyt, Ryanair, ..
* Differentiatie ('uniek product')
    1. De creatie van een unieke superieure 'voordelenbundel' (product).
    2. DIt leidt tot extra ksoten
    3. Maar verkopen tegen een hogere prijs
    * Bv luxewagens, high end kappers
-> Red Ocean Strategy! (er zal veel bloed vloeien door de marktstrijd)
Beter:
* Blue Ocean Strategy:
    * markt voor product zelf creeren
    * vervolgens zowel kostenleiderschap als differentiatie toepassen
    * Bv: apple
    * Hoe? Value innovation! 
        * Aan niet-klanten vragen waarom ze het niet kopen (klassiek marktonderzoek: "Waarom koopt u ons product"
        * Bv cirque du soleil

* Risico's op de grafiek:
* I:
    * Gevaar op imitatie door concurrenten
    * Te groot prijsverschil
* III: 
    * Snel verlies aan kostenvoordeel door technologische verandering (bv floppy's)
    * Te laat reageren op gewijzigde voorkeuren
* II:
    * Nooit doen! "Stuck in the middle!" 


* Hoe lage kosten realiseren? (op slide v gemiddelde prijs in de industrietak)
    1. Verwijderen alle extra's aan het product
    2. Design van het product vereenvoudigen
    3. Productie in lageloonlanden
    4. Verminderen vaste kosten
    5. Subsidies verwerven van de overheid

## EX: Wat wordt er bedoeld met een differentatiebasis? -> iets wat u onderscheid van uw concurrenten in een markt
Voorbelden differentiatiebasissen:
* Gebruiksgemak: Apple in de PC markt
* Design en prestaties: Mercedes Benz
* Betrouwbaarheid: Toyota auto's
* Gemakkelijke bereikbaarheid: Amazon
* Aparte smaak: Coca-Cola
* Veiligheid: Volvo
* Snelle productie op aanvraag: Dell

### Twee slides skippen! Tot 5.Concurentieverminderende strategie
* Strategische alliantie: samenwerking rond samen aankopen, distibutie product, ... BV: Danone en Unilever voor yoghurtijs
* Fusie: A + B => C
* Overname: A + B => A
* Joint venture: A + B richten samen firma C op (A & B blijven ook bestaan!). BV: BPost bank
* Kartel: prijsafspraken -> kei-illegaal!
* Verbonden beheersstructuren: geen extra :(

### ALLES VAN INTERNATIONALISERINGSSTRATEGIE VALT WEG! (Enkel gevalstudie van Danone blijft over)
#### Oefening Danone:
A. Voorbeeld van bedrijfsstrategie
B. topmanagement
C. Desinvestering


# Hoofdstuk 6: innovatiemanagement
* creativiteit = mogelijkheid om ideeen op unieke wijze te combineren
* innovatie = omzetten in winstgevende producten/processen
"Niet alles wat creatief is, is innovatief" Bv: machine om kromme bananen recht te trekken -> creatief, maar niet innovatief!

| Voorbeelden                                         |
|-----------------------------------------------------|
|  Mp3 speler, hybride auto, onlinebanking            |
| Automatisatie productielijn (robotten)              |
| Snellere computerchips (Intel)                      |
|  Zakrekenmachine, peer-to-peer netwerk              |
|  Lotus Speculoos koekjes Nieuwe processor van Intel |
| Draadloze communicatie                              |

* Distruptive innovation: zeer extreme vorm van radicale innovatie
* Bv: Taxisector wordt bedreigd door Uber
* Bank -> Google Wallet, Apple Pay
* Massive Open Online Courses (als concurrent voor KU Leuven)
* Cinema -> Netflix

### EX: verklaar kort, disruptive innovation (Het is een radicale vorm van innovate + de twee kenmerken + voorbeeld!)
### Slides 'gevalstudie' laten vallen tot aan 'Waarom is innovatie noodzakelijk?'

* extra opbrengsten te genereren: voorbeeld tijdslijn in facebook, facebook ive

a. Bonnen van innovatie: TECHNOLOGY PUSH INNOVATIE NIET VERWARREN MET PUSH&PULL INNOVATIE!
* technologypush Bv: DAB-radio als alternatief voor FM-radio
* market pull Bv: starbucks, website om bedrijven mee te delen

### Ex: Wat wordt er bedoeld met systeemperspectief van innovatie? -> In uw bedrijf moet ge de juiste omgeving creeren waarin innovatie kan gedijen!
-> enkel de manager veranderen is niet genoeg, de omgeving moet voldoen aan structurele, culturele en HR variabelen.
### Ex: twee van elke variabele kennen!

Mean climate scores:
* buitenste lijn is innovatief
* binnenste lijn is stagnerend
* middelste lijn is gemiddeld
Vooral risktaking is een belangrijke dimentie!

* Gatemodel: Innovatie verloopt via 5 fasen (STAGE)

### EX: leg kort uit, innovatietrechter van Schilling
-> van veel ideeen, naar 1 product
Bv pharma indusrie: gemiddelde doorloop v 12 jaar

### Alle slides na voorbeeld van mislukte innovaties tot h. levenscyclus van innovatie vallen weg! (9 tal slides?)

1. Introduction: prijs per eenheid groot (bv smartwatch)
2. Growth: prijs daalt (bv e-bike)
3. Maturity or stabilization: daalt, maar minder (bv gsm)
4. Decline: prijs kan wat alle kanten op (soms naar boven, soms naar onder, soms stabiel) (vinyl)

Definitie businessmodel: veel discussie hierover!
* Sleutelelement: hoe kan er geld mee worden verdiend?

* Advertentie gedreven: Google
* commissie gedreven: Ebay
* freemium: dropbox


# Hoofdstuk 7 marketingmanagement
### Ex: wat zijn de verschillen tussen business-to-business en business-to-consumer marketing?

|                                                    |  Business-to-business marketing (bv Materialise)              |  Busiess-to-consumer marketing (Bv Coca Cola)               |
|----------------------------------------------------|---------------------------------------------------------------|-------------------------------------------------------------|
| Omschrijving                                       |  Activiteiten spelen zich af tussen ondernemingen             |  Activiteiten spelen zich af tussen onderneming en individu |
|  Belang economisch rendement en rationele factoren | Hoog                                                          | Lager                                                       |
| Koopproces                                         | Formeel                                                       | Minder formeel                                              |
| Betrokken partijen                                 |  Deelname/invloed meerdere personen (gebruikers, inkopen, ..) | Beperkter                                                   |
|  Relaties tussen klant en koper                    | Hecht                                                         | Minder hecht                                                |


* Black box van de koper: Beinvloedende factoren op de koper & besluitvormingsproces van de ckoper
* a. De factoren die van invloed zijn op de koper NIET KENNEN 
* b. Per blokje van individueel leefpatroon (business-to-consumer) eentje kennen + toelichten
    * Sociale klasse: high-class, low-class
    * Referentiegroepen: groepen waarmee men zich graag vergelijkt (bv topsporters)
    * Leeften en levensfase: kuntsgebied -> consumenten kopen wat bij hun zelfbeeld past
    * Overtuigingen en attitudes: bv vegetariers

* Indeling van levensstijlen:
    * 1 levenstijl kennen en kunnen uitleggen

Rationele model van koopgedrag niet kennen, want consumenten gedragen zich zelden rationeel!

* Fase in het adoptieproces: ->enkel dit kennen hoezee
    * bestaat
    * informatie
    * overwegen
    * proberen
    *beslissen
-> doorloopsnelheid van het proces hangt af van zowel de persoon als de producteigenschappen

#### Slide voor producteigenschap en omschrijving valt weg
Relatief voordeel, compatibiliteit positief verband, deelbaarheid
complexiteit negatief verband

* Nudging: bv Holle Bolle Gijs, trap als piano (om roltrap te vermijden)


### Ex: wat wordt bedoeld met 'nudging' en geef een aantal voorbeelden + technieken

#### Alles tussen nudging en marketingmix valt weg! Euj

* De 4 p's van Marketing niet verwarren met de vijf p's van strategie!
A. productbeleid: 
* assortiment verwijst naar de breedte van het product, wat wordt wel en niet aangeboden? Bv: verschil tss Carrefour expess & Carrefour hypermarkt
* productkeuze verwijst naar de diepte: bv Delhaize biedt keiveel yoghurt soorten aan

B. plaatsbeleid:
* Ruimtelijk dwz plaats in de winkel: bv suiker helemaal achteraan in de winkel
* distributiekanalen: Hoe gerakn producten bij de klant?
* Onderste twee distributiekanalen zijn business-to-business
* rechtstreeks: bv Netflix, Facebook
* 1 tussenschakel: voeding
* 2 tussenschakels: drank
    * voordelen tussenschakels: extra expertise, nieuwe doelgroep
    * Nadelen: minder controle, opslagstromen, lagere marges 

### Ex: Welke distributievormen kan je onderscheiden in functie van het aantal verkooppunten in de kleinhandel?
* intensief zoals bv Coca-Cola
* selectief zoals bv televisie, huishoudtoestellen, ..
* exclusief zoals bv Dior, Prada

c.Prijsbeleid:
* Kostengerichte aanpak = kosten per eenheid + marge
* Concurrentiegerichte aanpak = prijs vergelijkbaar met concurrenten
* Vraaggerichte aanpak = wat wil je betalen?

| Prijszetting | Voorbeeld
| ------------------------------------ | ---------------------------------------------------------------- |
| Prijszetting voor kortingen | 2% korting bij betaling binnen 30 dagen | 
| Prijszetting voor prijsdiscriminatie | andere prijzen voor senioren, nachttarief elektriciteit |
| Psychologische prijszetting | het is 'duur, dus het is 'goed', supersolden (0.99 euro!) |
| Prijszetting door promotie |  Albert Hein | 
| Geografische prijszetting | cafe met zicht op monument |
| Internationale prijszetting | Gucci kost meer in Milaan dan in Ethiopie |
    
* Eigen prijselasticiteit: Wanneer de prijs verhoogd, gaat de verkoop in aantal eenheden afzwakken (mensen die niet meer kopen)
    * Hoe sterk reageert verkoop op prijsverhoging/verlaging?
    * "Percentage dat uitdrukt met hoeveel de vraag gaat wijzigen als de prijs stijg met 1%"
    * BV: als huurprijs 1% stijgt, zal de vraag dalen met 0.1%
    * BV: gevolgen op omzet: Situatie voor prijsstijging: 50 euro * 100 stuks = 5000 euro
        * na: 50.5 * 99.9 = 5044.95 euro

### Ex: Wat is eigen prijselasticiteit? Leg uit + reken uit adhv gegeven getallen 
